<div class="news-default-index container" style="padding: 0px 0 50px">
    <div class="col-md-11 no-padding" style="float: none;margin: 0 auto">
        <h1 style="text-align: center;font-size: 30px;padding: 30px 0;height: 100px;">آخرین اخبار</h1>
        <? foreach ($news as $item) {?>
        <a href="/news/default/show?id=<?=$item->id?>" style="margin-bottom: 20px;">
            <div class="item-news" style="display: inline-block;height: 200px;">
                <div class="col-md-4 no-padding">
                    <div class="col-md-12 no-padding" style="border-left: 1px solid #eee;">
                        <img class="col-md-12 no-padding" src="/images/uploads/news/<?=$item->image?>" style="border-radius: 0 6px 6px 0;height: 200px;width: 100%;background-color: #0A568C;" alt="">
                    </div>
                </div>
                <div class="col-md-8" style="height: 200px">
                    <div class="title-item-news" style="font-size: 16px">
                        <?=$item->title?>
                    </div>
                    <div class="desc-item-news" style="line-height: 1.6;"><?=$item->get_short_description(80)?></div>
                    <div class="details-item-news" style="margin:0;border-top: 1px solid #eee;position: absolute;bottom: 0;left: 0;width: 100%;">
                        <p class="date-comments-news col-md-12"
                           style="color: #bab4b2;margin-top: 2%;text-align: left;margin-bottom: 2%;font-size: 10px;">
                            <span>
                                <?=\app\classes\UsableFunctions::get_jalali_date($item->created_at, 'd F y')?>
                                <i class="fa fa-clock-o"></i>
                            </span>
                            <span class="comment-news" style="padding-right: 10px;">
                                <?=$item->views?$item->views:0?>
                                <i class="fa fa-eye"></i>
                            </span>
                            <i class="fa fa-share-alt pull-right"></i>
                        </p>
                    </div>
                </div>
            </div>
        </a>
<!--            <a href="/news/default/show?id=--><?//=$item->id?><!--" style="margin-bottom: 20px;">-->
<!--                <div class="item-news">-->
<!--                    <div class="col-md-4 no-padding">-->
<!--                        <img class="col-md-12 no-padding" src="/images/uploads/news/--><?//=$item->image?><!--" style="border-radius: 6px 6px 0 0;height: 220px;width: auto;background-color: #0A568C;" alt="">-->
<!--                    </div>-->
<!--                    <div class="col-md-8">-->
<!--                        <div class="title-item-news">-->
<!--                            --><?//=$item->title?>
<!--                        </div>-->
<!--                        <div class="desc-item-news">--><?//=$item->get_short_description(45)?><!--</div>-->
<!--                    </div>-->
<!--                    <div class="details-item-news">-->
<!--                        <p class="date-comments-news col-md-12"-->
<!--                           style="color: #bab4b2;margin-top: 4%;text-align: left;">-->
<!--                            <span>-->
<!--                                --><?//=\app\classes\UsableFunctions::get_jalali_date($item->created_at, 'd F y')?>
<!--                                <i class="fa fa-clock-o"></i>-->
<!--                            </span>-->
<!--                            <span class="comment-news" style="padding-right: 10px;">-->
<!--                                --><?//=$item->views?$item->views:0?>
<!--                                <i class="fa fa-eye"></i>-->
<!--                            </span>-->
<!--                            <i class="fa fa-share-alt pull-right"></i>-->
<!--                        </p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </a>-->
        <?}?>
    </div>
</div>
