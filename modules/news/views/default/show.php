<style>
    .top-news p, .cat-news p {
        color: #000;
    }
</style>
<div class="news-default-index container" style="padding: 50px 0 50px">
    <div class="col-md-10 no-padding" style="float: none;margin: 0 auto">
        <h1 class="col-md-12" style="text-align:center;font-size:25px;height: 32px;margin-bottom: 20px"><?=$news->title?></h1>
        <div class="col-md-12 no-padding cat-news" style="cursor: auto;background: #fff;border-radius: 6px;">
            <img class="col-md-12 no-padding" style="width:100%;border-radius: 5px 5px 0 0;" src="/images/uploads/news/<?=@$news->image?>" alt="">
<!--                --><?//=@$news->tags[0]?'<div class="tag tag-yellow">'.$news->tags[0]->title.'</div>':'';?>
<!--            </div>-->
            <div class="col-md-12 no-padding" style="padding-top: 10px !important;text-align: left">
                <p class="date-comments-news col-md-3 pull-left">
                    <span>
                        <?=\app\classes\UsableFunctions::get_jalali_date($news->created_at, 'd F y')?>
                        <i class="fa fa-clock-o"></i>
                    </span>
                    <span class="comment-news">
                        <?=$news->views?>
                        <i class="fa fa-commenting-o"></i>
                    </span>
<!--                    <i class="fa fa-share-alt pull-right"></i>-->
                </p>

            </div>
            <div class="col-md-12" style="line-height: 25px;padding: 20px; color: #000 !important;">
                <?=$news->description?>
            </div>
        </div>
    </div>
</div>
