<?php

namespace app\modules\news\models;

use Yii;

/**
 * This is the model class for table "news_comment".
 *
 * @property integer $id
 * @property integer $news_id
 * @property string $full_name
 * @property string $email
 * @property string $content
 * @property integer $created_at
 *
 * @property News $news
 */
class NewsComment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_id', 'created_at'], 'integer'],
            [['content'], 'string'],
            [['full_name', 'email'], 'string', 'max' => 255],
            [['news_id'], 'exist', 'skipOnError' => true, 'targetClass' => News::className(), 'targetAttribute' => ['news_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'news_id' => 'News ID',
            'full_name' => 'Full Name',
            'email' => 'Email',
            'content' => 'Content',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::className(), ['id' => 'news_id']);
    }
}
