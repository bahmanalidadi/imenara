<?php

namespace app\modules\news\controllers;

use app\modules\news\models\News;
use Yii;
use yii\web\Controller;

/**
 * Default controller for the `news` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $news = News::find()->where('available = 1')->orderBy('id DESC')->all();
        $this->layout = '/page';
        return $this->render('index', ['news' => $news]);
    }

    public function actionShow(){
        $id = Yii::$app->request->get('id');
        $news = News::find()->where(['id' => $id])->one();
        $news->views += 1;
        $news->save();
        $this->layout = '/page';
        return $this->render('show', ['news' => $news]);
    }
}
