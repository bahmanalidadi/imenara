<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news_category`.
 */
class m180327_062905_create_news_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news_category', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'available' => $this->boolean()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('news_category');
    }
}
