<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tag_news`.
 */
class m180426_050528_create_tag_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tag_news', [
            'id' => $this->primaryKey(),
            'news_id' => $this->integer(),
            'tag_id' => $this->integer(),
        ]);

        $this->createIndex('idx-news_tag_news_id',
            'tag_news',
        'news_id'
            );
        $this->addForeignKey('fk-news_tag_news_id',
            'tag_news',
        'news_id',
        'news',
        'id',
        'CASCADE',
        'CASCADE'
        );
        $this->createIndex('idx-tag_tag_news_id',
            'tag_news',
        'tag_id'
            );
        $this->addForeignKey('fk-tag_tag_news_id',
            'tag_news',
        'tag_id',
        'tag',
        'id',
        'CASCADE',
        'CASCADE'
            );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-tag_tag_news_id','tag_news');
        $this->dropIndex('idx-tag_tag_news_id','tag_news');
        $this->dropForeignKey('fk-news_tag_news_id','tag_news');
        $this->dropIndex('idx-news_tag_news_id','tag_news');
        $this->dropTable('tag_news');
    }
}
