<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news`.
 */
class m180327_063017_create_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'image' => $this->string(),
            'title' => $this->string(),
            'description' =>$this->text(),
            'created_at' => $this->integer(),
            'views' => $this->integer(),
            'available' => $this->boolean()
        ]);
        $this->createIndex('idx-news_category', 'news', 'category_id');
        $this->addForeignKey(
            'fk-news_category',
            'news',
            'category_id',
            'news_category',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-news_category', 'news');
        $this->dropIndex('idx-news_category', 'news');
        $this->dropTable('news');
    }
}
