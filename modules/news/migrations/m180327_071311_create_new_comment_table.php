<?php

use yii\db\Migration;

/**
 * Handles the creation of table `new_comment`.
 */
class m180327_071311_create_new_comment_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news_comment', [
            'id' => $this->primaryKey(),
            'news_id' => $this->integer(),
            'full_name' => $this->string(),
            'email' => $this->string(),
            'content' => $this->text(),
            'created_at' => $this->integer(),
        ]);

        $this->createIndex('idx-comment_news_id', 'news_comment', 'news_id');
        $this->addForeignKey(
            'fk-comment_news_id',
            'news_comment',
            'news_id',
            'news',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-comment_news_id', 'news_comment');
        $this->dropIndex('idx-comment_news_id', 'news_comment');
        $this->dropTable('news_comment');
    }
}
