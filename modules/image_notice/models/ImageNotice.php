<?php

namespace app\modules\image_notice\models;

use Yii;

/**
 * This is the model class for table "image_notice".
 *
 * @property int $id
 * @property string $image
 * @property int $created_at
 */
class ImageNotice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'image_notice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at'], 'integer'],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'شماره',
            'image' => 'تصویر',
            'created_at' => 'تاریخ ایجاد',
        ];
    }
}
