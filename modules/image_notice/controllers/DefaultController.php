<?php

namespace app\modules\image_notice\controllers;

use yii\web\Controller;

/**
 * Default controller for the `image_notice` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
