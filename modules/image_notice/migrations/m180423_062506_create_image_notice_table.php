<?php

use yii\db\Migration;

/**
 * Handles the creation of table `image_notice`.
 */
class m180423_062506_create_image_notice_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('image_notice', [
            'id' => $this->primaryKey(),
            'image' => $this->string(),
            'created_at' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('image_notice');
    }
}
