
<?php

use yii\bootstrap\BootstrapAsset;

?>

<!doctype html>
    <!--[if lt IE 7 ]> <html lang="en" class="ie6"> <![endif]-->
    <!--[if IE 7 ]>    <html lang="en" class="ie7"> <![endif]-->
    <!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
    <!--[if IE 9 ]>    <html lang="en" class="ie9"> <![endif]-->
    <!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <head>
        <meta name="viewport" content="width = 1050, user-scalable = no" />
        <script type="text/javascript" src="/extras/jquery.min.1.7.js"></script>
        <script type="text/javascript" src="/extras/modernizr.2.5.3.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<!--        <link rel="stylesheet" href="css/bootstrap.min.css">-->

        <style>
            @import url('/css/fontiran.css');
            body {
                font-family: IRANSans !important;
                font-weight: 300;
                direction: ltr;
                background-color: #ffffff;
                margin: 0;
            }
            h1, h2, h3, h4, h5, h6,input, textarea {
                font-family: IRANSans !important;
            }
            .nav-top {
                border-radius: 6px;
                background-color: rgb(255, 255, 255);
                box-shadow: 0px 3px 25px 0px rgba(32, 42, 54, 0.47);
                width: 1140px;
                margin: 32px auto 0;
                height: 59px;
                display: flex;
                flex-flow: row;
                direction: rtl;
            }

            .nav-top .item {
                padding-top: 15px;
                text-align: center;
            }

            .logo-fa {
                background-image: url('/images/Layer 802.png');
                width: 138px;
                height: 39px;
                margin: 8px;
                background-repeat: no-repeat;
            }

            .nav-top .item a{
                color: #202a36;
                text-decoration: none;
                font-size: 18px;

            }
            .nav-top .item:hover a{
                color: #3d96b5;
            }
        </style>
    </head>
    <body>
    <div class="container-fluid col-md-12" style="height: 120px;background-color: #FFF;width: 100%;">
        <div class="container nav-top">
            <div class="col-md-2 item pull-right logo-fa"><a href=""></a></div>
            <div class="col-md-1 item pull-right"><a href="">خانه</a></div>
            <div class="col-md-1 item pull-right"><a href="/shop">محصولات</a></div>
            <div class="col-md-2 item pull-right"><a href="">جدب کارشناس</a></div>
            <div class="col-md-1 item pull-right"><a href="">آموزش</a></div>
            <div class="col-md-2 item pull-right"><a href="">تماس با ما</a></div>

        </div>
    </div>

    <div class="flipbook-viewport">
        <div class="container">
            <div class="flipbook">
                <?php
                $i = 0;
                foreach ($catalog_images as $catalog_image) {?>
                    <div class="<?=$i == 0 || $i == (count($catalog_images)-1) ? 'page':'double'?>" style="background-image:url(<?='/images/uploads/catalog/'.$catalog_image->image?>)"></div>
                <? $i++;
                }?>
            </div>
        </div>
    </div>

<!--    <script type="text/javascript">-->
<!---->
<!--        function loadApp() {-->
<!---->
<!--            // Create the flipbook-->
<!---->
<!--            $('.flipbook').turn({-->
<!--                // Width-->
<!---->
<!--                width:922,-->
<!---->
<!--                // Height-->
<!---->
<!--                height:600,-->
<!---->
<!--                // Elevation-->
<!---->
<!--                elevation: 50,-->
<!---->
<!--                // Enable gradients-->
<!---->
<!--                gradients: true,-->
<!---->
<!--                // Auto center this flipbook-->
<!---->
<!--                autoCenter: true-->
<!---->
<!--            });-->
<!--        }-->
<!---->
<!--        // Load the HTML4 version if there's not CSS transform-->
<!---->
<!--        yepnope({-->
<!--            test : Modernizr.csstransforms,-->
<!--            yep: ['/lib/turn.js'],-->
<!--            nope: ['/lib/turn.html4.min.js'],-->
<!--            both: ['/css/basic.css'],-->
<!--            complete: loadApp-->
<!--        });-->
<!---->
<!--    </script>-->



    <script type="text/javascript">

        function loadApp() {

            var flipbook = $('.flipbook');

            // Check if the CSS was already loaded

            if (flipbook.width()==0 || flipbook.height()==0) {
                setTimeout(loadApp, 10);
                return;
            }

            $('.flipbook .double').scissor();

            // Create the flipbook

            $('.flipbook').turn({
                // Elevation

                elevation: 50,
                // dir: rtl,

                // Enable gradients

                gradients: true,

                // Auto center this flipbook

                autoCenter: true

            });
        }

        // Load the HTML4 version if there's not CSS transform

        yepnope({
            test : Modernizr.csstransforms,
            yep: ['/lib/turn.min.js'],
            nope: ['/lib/turn.html4.min.js'],
            both: ['/lib/scissor.min.js', '/css/double-page.css'],
            complete: loadApp
        });

    </script>

    </body>
</html>