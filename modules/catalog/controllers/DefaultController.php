<?php

namespace app\modules\catalog\controllers;

use app\modules\catalog\models\Catalog;
use yii\web\Controller;

/**
 * Default controller for the `catalog` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = '/none';
        $catalog_images = Catalog::find()->orderBy('id ASC')->all();
        return $this->render('index', ['catalog_images' => $catalog_images]);
    }
}
