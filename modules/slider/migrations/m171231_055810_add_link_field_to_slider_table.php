<?php

use yii\db\Migration;

class m171231_055810_add_link_field_to_slider_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('slider', 'link', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('slider', 'link');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171231_055810_add_link_field_to_slider_table cannot be reverted.\n";

        return false;
    }
    */
}
