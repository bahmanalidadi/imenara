<?php

namespace app\modules\slider\models;

use Yii;

/**
 * This is the model class for table "slider".
 *
 * @property integer $id
 * @property string $image
 * @property string $title
 * @property string $description
 * @property integer $position
 * @property integer $available
 * @property string $link
 */
class Slider extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image'], 'required'],
            [['description'], 'string'],
            [['position', 'available'], 'integer'],
            [['image', 'title', 'link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Image',
            'title' => 'Title',
            'description' => 'Description',
            'position' => 'Position',
            'available' => 'Available',
            'link' => 'Link',
        ];
    }
}
