<?php

namespace app\modules\admin\controllers;

use app\modules\poll\models\PollItem;
use Yii;
use app\modules\poll\models\Poll;
use app\modules\poll\models\PollSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PollController implements the CRUD actions for Poll model.
 */
class PollController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Poll models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PollSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Poll model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Poll model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Poll();
        $items = Yii::$app->request->post('PollItem');
//        print_r($items);die;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            foreach ($items as $item){
                if ($item['item']!=''){
                    $poll_item = new PollItem();
                    $poll_item->item = $item['item'];
                    $model->link('pollItems', $poll_item);
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Poll model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $items = Yii::$app->request->post('Poll');
        if (Yii::$app->request->isPost) {

            $poll_id = [];
            foreach ($items['pollItems'] as $item) {

                if ($item['id']) {
                    $poll_id[] = $item['id'];
                }
            }

            foreach ($model->pollItems as $poll_item){
                if (!in_array($poll_item->id,$poll_id)){
                    $poll_item->delete();
                }
            }

            foreach ($items['pollItems'] as $item){

                if ($item['id']){
                    $poll_id[] = $item['id'];
                    $poll_item = PollItem::find()->where(['id'=>$item['id']])->one();
                    $poll_item->item = $item['item'];
                    $poll_item->save();
                }
                else{
                    $poll_item = new PollItem();
                    $poll_item->item = $item['item'];
                    $model->link('pollItems', $poll_item);
                }
            }


            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Poll model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Poll model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Poll the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Poll::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
