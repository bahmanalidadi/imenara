<?php

namespace app\modules\admin\controllers;

use app\modules\product\models\CategoryProp;
use Yii;
use app\modules\product\models\ProductCategory;
use app\modules\product\models\ProductCategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductCategoryController implements the CRUD actions for ProductCategory model.
 */
class ProductCategoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProductCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProductCategory model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProductCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductCategory();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $data = Yii::$app->request->post();
            foreach ($data['property'] as $key =>$item){
                $cat_prop = new CategoryProp();
                $cat_prop->name = $item;
                $cat_prop->parent_id = $data['parent_name'][$key];

                $cat_prop->link('category', $model);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ProductCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
//        print_r(Yii::$app->request->post());die;
        $model = $this->findModel($id);
        $category_prop = CategoryProp::find()->where(['category_id' => $model->id])->all();
        $selected_category_prop_id = [];
        $category_prop_post_id = [];
        foreach ($category_prop as $item){
            $selected_category_prop_id[]=$item->id;
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $data = Yii::$app->request->post();
            foreach ($data['property'] as $key =>$item){
                $category_prop_post_id[] = $key;

            }
            foreach ($model->categoryProps as $item){
                if (!in_array($item->id , $category_prop_post_id)){
                    $item->delete();
                }
            }
            foreach ($data['property'] as $key =>$item){
                if (in_array($key , $selected_category_prop_id)){
                    $cat_prop = CategoryProp::find()->where(['id'=>$key])->one();
                }
                else{
                    $cat_prop = new CategoryProp();
                }
                $cat_prop->name = $item;
                $cat_prop->parent_id = $data['parent_name'][$key];
                $cat_prop->link('category', $model);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ProductCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProductCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductCategory::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
