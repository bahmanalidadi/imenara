<?php

namespace app\modules\admin\controllers;

use app\models\UploadForm;
use app\modules\product\models\CategoryProp;
use app\modules\product\models\ParentCategoryProp;
use app\modules\product\models\ProductImage;
use app\modules\product\models\ProductProp;
use Yii;
use app\modules\product\models\Product;
use app\modules\product\models\ProductCategory;
use app\modules\product\models\ProductSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $parent = ParentCategoryProp::find()->all();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'parent_cat_prop' => $parent,
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();
        $product_prop = Yii::$app->request->post('property');

//        if(Yii::$app->request->isPost)
//        {
//            print_r($_FILES);die;
//        }

        if ($model->load(Yii::$app->request->post())&& $model->save() ) {
                foreach ($product_prop as $key => $item){
                    $product_property = new ProductProp();
                    $product_property->category_prop_id = $key;
                    $product_property->value = $item;
                    $product_property->link('product',$model);
                }
                return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $product_prop = Yii::$app->request->post('property');


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            foreach ($product_prop as $key => $item){
                $product_property = ProductProp::find()->where(['category_prop_id'=>$key, 'product_id' => $model->id])->one();
                if (!$product_property){
                    $product_property = new ProductProp();
                    $product_property->product_id = $model->id;
                }
                $product_property->category_prop_id = $key;
                $product_property->value = $item? $item : null;
                $product_property->save();
            }

            if(Yii::$app->request->isPost)
            {
                if(@$_FILES['files']['name'][0] != ''){
                    $model_file = new UploadForm();
                    $model_file->multipleFiles = UploadedFile::getInstancesByName('files');
                    $files_name[] = $model_file->upload('../web/images/uploads/product/');

                    foreach ($files_name[0] as $name) {
                        $product_image = new ProductImage();
                        $product_image->name = $name;
                        $product_image->product_id = $model->id;
                        $product_image->save();
                    }
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionAddProperties()
    {
        $cat_props = Yii::$app->request->post('cat_props');
        if ($cat_props){
            $data = Yii::$app->request->post();
            $props = [];
            foreach ($cat_props as $key =>$item){
                $category_prop = new CategoryProp();
                $category_prop->name = $item;
                $category_prop->category_id = Yii::$app->request->post('id');
                $category_prop->parent_id = $data['parents'][$key];
                $category_prop->save();
                $props[] = $category_prop;
            }
            Yii::$app->response->format = Response::FORMAT_JSON;
            Yii::$app->response->statusCode = 200;
            return $props;
        }
        Yii::$app->response->statusCode = 400;
        return "دسته بندی یافت نشد";

    }

    public function actionUploadImage()
    {
        $file_name = '' ;
        if (Yii::$app->request->isPost){
            $model_file = new UploadForm();
            $model_file->imageFile = UploadedFile::getInstanceByName('file');
            $file_name = $model_file->upload('../web/images/uploads/product/');
        }
        return $file_name;
    }

    public function actionDeleteImage()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = Yii::$app->request->post('id');
        if(ProductImage::find()->where(['id' => $id])->one()->delete())
        {
            Yii::$app->response->statusCode = 200;
            return [
                'id' => $id,
                'message' => 'product image was deleted'
            ];
        }
        Yii::$app->response->statusCode = 400;
        return false;
    }
}
