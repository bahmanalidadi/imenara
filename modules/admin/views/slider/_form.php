<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Slider */
/* @var $form yii\widgets\ActiveForm */
$host_name = 'http://'.str_replace('backend.','',$_SERVER['SERVER_NAME']);
?>

<div class="slider-form">

    <?php $form = ActiveForm::begin(); ?>

    <?if($model->image){?>
        <img width="300" src="<?=$host_name.'/images/uploads/slider/'.$model->image?>" >
    <?}?>

    <?= $form->field($model, 'image', ['options' => ['style' => 'direction: rtl; text-align:right; margin: 20px 0;']])
        ->fileInput() ?>

<!--    --><?//= $form->field($model, 'image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'link')->textInput() ?>

    <?= $form->field($model, 'position')->textInput(['type' => 'number']) ?>

    <?= $form->field($model, 'available')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('ذخیره', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
