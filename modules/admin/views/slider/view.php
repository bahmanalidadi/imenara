<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Slider */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Sliders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-view">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->

    <p>
        <?= Html::a('ویرایش', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('حذف', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label' => 'تصویر',
                'format' => 'raw',
                'value' => function($data){
                    $host_name = 'http://'.str_replace('backend.','',$_SERVER['SERVER_NAME']);
                    return '<img width="300" src="'.$host_name.'/images/uploads/slider/'.$data->image.'" >';
                },
            ],
            'title',
            'link',
            'description:ntext',
            'position',
            [
                'label' => 'فعال',
                'format' => 'raw',
                'value' => function($data){
                    if($data->available == 1){
                        return '<input disabled type="checkbox" checked="checked">';
                    }
                    return '<input disabled type="checkbox">';
                },
            ],
        ],
    ]) ?>

</div>
