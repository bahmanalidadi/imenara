<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\model_search\SearchSlider */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'مدیریت اسلایدر';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-index">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('ایجاد اسلاید جدید', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'layout'=>"{items}\n{pager}",
        'columns' => [
            'id',
            [
                'class' => 'yii\grid\DataColumn',
                'label' =>'تصویر',
                'format' => 'raw',
                'value' => function ($data) {
                    $host_name = 'http://'.str_replace('backend.','',$_SERVER['SERVER_NAME']);
                    return '<img width="100" src="'.$host_name.'/images/uploads/slider/'.$data->image.'" >';
                },
            ],
            'title',
            [
                'class' => 'yii\grid\DataColumn',
                'label' =>'توضیحات',
                'format' => 'raw',
                'value' => function ($data) {
                    return substr($data->description, 0 , 150)."...";
                },
            ],
            'link',
            'position',
            [
                'class' => 'yii\grid\DataColumn',
                'label' =>'فعال',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->available == 1 ? '<i class="fa fa-check-circle" style="color: #4caf50"></i>' : '<i class="fa fa-times-circle" style="color: #ff5722"></i>';
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
