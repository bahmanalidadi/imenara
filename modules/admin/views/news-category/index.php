<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\news\models\search_models\NewsCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'دسته بندی های اخبار';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-category-index">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('ایجاد دسته بندی', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'layout'=>"{items}\n{pager}",
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            [
                'class' => 'yii\grid\DataColumn',
                'label' =>'در دسترس',
                'format' => 'raw',
                'value' => function ($data) {
                    $html = '<i class="fa fa-times" style="color: #F00;font-size:20px"></i>';
                    if($data->available)
                    {
                        $html = '<i class="fa fa-check" style="color: #0F0;font-size:20px"></i>';
                    }
                    return $html;
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
