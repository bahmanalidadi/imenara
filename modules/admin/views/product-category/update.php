<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\product\models\ProductCategory */

$this->title = 'ویرایش :'.$model->name;
$this->params['breadcrumbs'][] = ['label' => 'Product Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-category-update">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->
<div class="col-md-8">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<div style="clear: both;"></div>

</div>
