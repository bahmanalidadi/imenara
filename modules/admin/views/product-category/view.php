<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\product\models\ProductCategory */
//print_r($model->propertyParents);die;
$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Product Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="product-category-view">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->

    <p>
        <?= Html::a('ویرایش', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('حذف', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
<div class="col-md-6">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
//            'created_at',
        ],
    ]) ?>

</div>
    <div class="col-md-6">

        <h3 class="col-md-12" style="font-size:20px;margin-bottom: 10px;color: #1c2d3f;">ویژگی ها</h3>

        <table class="table table-striped" style="margin-left: 1px;">
            <tbody>
                <?foreach ($model->propertyParents as $prop_parent){?>
                    <tr>
                        <td style="background-color: #2d2d2d; color: white;"><?=$prop_parent->name?></td>
                    </tr>
                    <?foreach ($prop_parent->categoryProps as $item) {?>
                    <tr>
                        <td><?= $item->name ?></td>
                    </tr>
                    <?}
                }?>
            </tbody>
        </table>
    </div>
<div style="clear: both;"></div>
</div>
