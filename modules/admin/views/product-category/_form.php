<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\product\models\ProductCategory */
/* @var $form yii\widgets\ActiveForm */
$parents = \app\modules\product\models\ParentCategoryProp::find()->all();
?>

<div class="product-category-form" style="padding-top: 50px;">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>



    <table class="table col-md-12">
        <thead>
        <tr>
            <th scope="col">شماره</th>
            <th scope="col">خصوصیات</th>
            <th scope="col">دسته</th>
        </tr>
        </thead>
        <tbody id="table-row">
        <?if ($model->isNewRecord & $model->categoryProps){?>
        <tr id="0">
            <th></th>
            <td>
                <input type="text" value="" class="form-control"
                       name="property[]">
            </td>
            <td>
                    <select name="parent_name[]">
                        <?foreach ($parents as $parent){?>
                            <option value="<?=$parent->id?>"><?=$parent->name?></option>
                        <?}?>
                    </select>
            </td>
            <td><button class="btn btn-danger" type="button" rel="0" id="delete">حذف</button></td>
        </tr>
        <?}?>
            <select id="parent" style="display: none">
                <?foreach ($parents as $parent){?>
                        <option value="<?=$parent->id?>" ><?=$parent->name?></option>
                 <?}?>
            </select>
        <? if (!$model->isNewRecord & $model->categoryProps) {?>

            <?foreach ($model->categoryProps as $item) {
                ?>
                <tr id="<?= $item->id ?>">
                    <th><?= $item->id ?></th>
                    <td>
                        <input type="text" value="<?= $item->name ?>" class="form-control"
                               name="property[<?= $item->id ?>]">
                    </td>
                    <td>
                        <select name="parent_name[<?= $item->id ?>]">
                            <?foreach ($parents as $parent){?>
                                <option value="<?=$parent->id?>" <?=$item->parent_id == $parent->id ?'selected' :''?> ><?=$parent->name?></option>
                            <?}?>
                        </select>
                    </td>
                    <td><button class="btn btn-danger" type="button" rel="<?= $item->id ?>" id="delete">حذف</button></td>
                </tr>
                <?
            }
        } ?>
        </tbody>
        <tfoot class="table_footer">

        </tfoot>
    </table>

    <div class="form-group col-md-12">
        <?= Html::submitButton('ذخیره', ['class' => 'btn btn-success']) ?>
    </div>
    <div style="clear:both;"></div>

    <?php ActiveForm::end(); ?>
<?php
$this->registerJs(<<<JS
    $(document).ready(function() {
        var str = '<tr>';
            str += '     <th scope="row"></th>';
            str += '     <td><button class="btn btn-primary" type="button" id="add">اضافه کردن</button></td>';
            str += '     <td></td>';
            str += '</tr>';
        $('.table_footer').append(str);
    });

        var i = 1;
    $(document).on('click', '#add', function() {
        var str = '<tr id = '+i+'>';
            str += '     <th scope="row"></th>';
            str += '     <td><input type="text" class="form-control cat_prop" name="property[]"></td>';
            str += '     <td><select name="parent_name[]">'+$('#parent').html()+'</select></td>';
            str += '     <td><td><button class="btn btn-danger" type="button" rel="'+i+'" id="delete">حذف</button></td>';
            str += '</tr>';
          i++;  
        $('#table-row').append(str);
   });
    $(document).on('click','#delete',function() {
        var id = $(this).attr('rel');
        $('#'+id+'').remove();
    });
       
       
           
JS
    , \yii\web\View::POS_READY)
?>

</div>
