<?php

use kartik\file\FileInput;
use kartik\select2\Select2;
use xj\dropzone\Dropzone;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\product\models\Product */
/* @var $form yii\widgets\ActiveForm */
$parents = \app\modules\product\models\ParentCategoryProp::find()->all();
$cats = \app\modules\product\models\ProductCategory::find()->orderBy('id ASC')->all();
$cat_props = \app\modules\product\models\CategoryProp::find()->where(['category_id' =>$cats[0]->id])->all();
?>
<style>
    .delete_image{
        position: absolute;
        bottom: -25px;
        color: #FFF;
        background-color: #F00;
        width: 100%;
        height: 25px;
        text-align: center;
        font-size: 14px;
        line-height: 26px;
        cursor: pointer;
    }

    .standard-icons div{
        height: 100px;
        background-color: #fff;
        border: 1px solid #ccc;
        display: table;
    }
</style>
<?php $form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data'
    ]
]); ?>
<div class="col-md-6" style="margin-top: 20px">

    <div class="product-form">
        <div class="col-md-12">
            <?= $form->field($model, 'category_id')->dropDownList(\yii\helpers\ArrayHelper::map($cats, 'id', 'name')) ?>
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'model')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'series')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'guaranty')->textInput(['type' => 'number']) ?>
            <?= $form->field($model, 'description')->textarea(['dir' => 'auto']) ?>
        </div>

    <!--    --><? //= $form->field($model, 'available')->textInput() ?>
        <table class="table col-md-12">
            <thead>
            <tr>
                <th scope="col">شماره</th>
                <th scope="col">خصوصیات</th>
                <th scope="col">مقادیر</th>
            </tr>
            </thead>
            <tbody id="table-row">
            <select id="parent" style="display: none">
                <?foreach ($parents as $parent){?>
                    <option value="<?=$parent->id?>" ><?=$parent->name?></option>
                <?}?>
            </select>

           <?php foreach ($cat_props as $item) {
               $value = '';
               if(!$model->isNewRecord){
                   $value = $model->get_prop_value($item->id);
               }
               ?>
               <tr>
                   <th><?= $item->id ?></th>
                   <td><?= $item->name ?></td>
                   <td>
                       <input type="text" value="<?=$value?>" class="form-control"
                              name="property[<?= $item->id ?>]">
                   </td>
               </tr>
           <?}?>

    <!--        --><?// if (!$model->isNewRecord & $model->productProps) {
    //
    //            foreach ($model->productProps as $item) {
    //                ?>
    <!--                <tr>-->
    <!--                    <th>--><?//= $item->categoryProp->id ?><!--</th>-->
    <!--                    <td>--><?//= $item->categoryProp->name ?><!--</td>-->
    <!--                    <td>-->
    <!--                        <input type="text" value="--><?//= $item->value ?><!--" class="form-control"-->
    <!--                               name="property[--><?//= $item->categoryProp->id ?><!--]">-->
    <!--                    </td>-->
    <!--                </tr>-->
    <!--            --><?//
    //            }
    //        }?>
            </tbody>
            <tfoot class="table_footer">
            <tr>
                <th scope="row"></th>
                <td>
                    <button class="btn btn-danger"  type="button" id="save" style="display: none">ذخیره</button>
                    <button class="btn btn-primary" type="button" id="add">اضافه کردن</button>
                </td>
                <td></td>
            </tr>

            </tfoot>
        </table>

        <div style="clear: both;"></div>









        <div class="standard-icons">
            <div class="col-md-3">
                <img src="/images/Guarantee.tif" alt="" class="col-md-12">
            </div>
            <div class="col-md-3">
                <img src="/images/IMENARA CE.jpg" alt="" class="col-md-12">
            </div>
            <div class="col-md-3">
                <img src="/images/IMENARA FCC.jpg" alt="" class="col-md-12">
            </div>
            <div class="col-md-3">
                <img src="/images/IMENARA ISO.jpg" alt="" class="col-md-12">
            </div>
            <div class="col-md-3">
                <img src="/images/IMENARA ISO10002.jpg" alt="" class="col-md-12">
            </div>
            <div class="col-md-3">
                <img src="/images/IMENARA ISO10004.jpg" alt="" class="col-md-12">
            </div>
            <div class="col-md-3">
                <img src="/images/IMENARA RoHs.jpg" alt="" class="col-md-12">
            </div>
            <div class="col-md-3">
                <img src="/images/logo onvif.jpg" alt="" class="col-md-12">
            </div>
        </div>

        <div style="clear: both;"></div>


        <div class="form-group col-md-12">
            <?= Html::submitButton('ذخیره', ['class' => 'btn btn-success']) ?>
        </div>

        <div style="clear: both;"></div>

    </div>
</div>

    <div class="col-md-6" style="margin-top: 20px">
        <h2 style="margin: 0px 0 10px; font-size: 18px;">تصاویر</h2>
        <div class="col-md-12 no-padding" style="margin-bottom: 50px">

        <?php
            foreach ($model->productImages as $item) {?>
                <div class="col-md-4">
                    <div class="col-md-12 no-padding">
                        <a href="/images/uploads/product/<?= $item->name ?>" target="_blank">
                            <img width="130" height="100" class="col-md-12 no-padding" src="/images/uploads/product/<?=$item->name?>" alt="">
                        </a>
                        <div class="delete_image" rel="<?=$item->id?>">
                            <i class="fa fa-trash"></i>
                            حذف
                        </div>
                    </div>
                </div>
            <?}?>
        </div>
        <div style="clear:both;"></div>

        <?php
            echo FileInput::widget([
                'name' => 'files[]',
                'options' => ['multiple' => true, 'accept' => 'image/*'],
                'pluginOptions' => [
                    'initialPreviewAsData'=>true,
                    'initialCaption'=>"",
                    'initialPreviewConfig' => [],
                    'overwriteInitial'=>false,
                    'maxFileSize'=>2800
                ]
            ]);
        ?>
    </div>
    <div style="clear: both;"></div>
<?php ActiveForm::end(); ?>



    <?php
    $this->registerJs(<<<JS
    
        $('#product-category_id').change(function () {
            $('#table-row tr').remove();
            $.ajax({
                url : "/site/get-properties",
                method : "post",
                dataType : "json",
                data : {
                    id : $(this).val(),
                },
                success : function(data){
                    for(var i= 0;i<data.length;i++){
                        var str = '<tr>';
                        str += '     <th scope="row">'+data[i].id+'</th>';
                        str += '     <td>'+data[i].name+'</td>';
                        str += '     <td><input type="text" class="form-control" name="property['+data[i].id+']"></td>';
                        str += '</tr>';
                        
                        $('#table-row').append(str);
                    }
                }
            });
        });
      $(document).on('click', '#add', function() {
           var str = '<tr>';
                str += '     <th scope="row"></th>';
                str += '     <td><input type="text" class="form-control cat_prop" name="cat_prop"></td>';
                str += '     <td><select class="parent" name="parent_name[]">'+$('#parent').html()+'</select></td>';
                str += '</tr>';
                
            $('#table-row').append(str);
            $('#save').show();
            
       });
      // <select name="parent_name[]">'+$('#parent').html()+'</select>
            
    $(document).on('click', '#save', function() {
        var probs = [];
        var parents = [];
        $('.cat_prop').each(function() {
            probs.push($(this).val());
        });
        $('.parent').each(function() {
          parents.push($(this).val());
        });
        $('#save').hide();
        $.ajax({
            url : "/admin/product/add-properties",
            method : "post",
            dataType : "json",
            data : {
                id :$('#product-category_id').val(),
                cat_props : probs ,
                parents : parents,
            },
            success : function(data){
                $('.cat_prop').parents('tr').remove();
                for(var i= 0;i<data.length;i++){
                        var str = '<tr>';
                        str += '     <th scope="row">'+data[i].id+'</th>';
                        str += '     <td>'+data[i].name+'</td>';
                        str += '     <td><input type="text" class="form-control" name="property['+data[i].id+']"></td>';
                        str += '</tr>';
                        $('#table-row').append(str);
                    }
            }
        });
    });
    
    $(document).on('click', '.delete_image', function() {
        var id = $(this).attr('rel');
        $.ajax({
            url : "/admin/product/delete-image",
            method : "post",
            dataType : "json",
            data : {
                id :id
            },
            success : function(data){
                $('.delete_image[rel='+data.id+']').parent().parent().remove();
            }
        });
    });
    
    $('.fileinput-remove').remove();

    
JS
        , \yii\web\View::POS_READY)
    ?>

<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea' });</script>