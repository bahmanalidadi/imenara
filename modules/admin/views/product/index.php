<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\product\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'محصولات';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('ایجاد محصول', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'layout'=>"{items}\n{pager}",
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'id',
//            'available',
//            'category_id',
            [
                'class' => 'yii\grid\DataColumn',
                'label' =>'دسته بندی',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->category->name;
                },
            ],
            'name',
            'model',
            'series',
//            'description',



            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
