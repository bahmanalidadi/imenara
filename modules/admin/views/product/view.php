<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\product\models\Product */
//print_r();die;
$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->

    <p  style="margin-bottom: 20px">
        <?= Html::a('ویرایش', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('حذف', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <div class="col-md-6" style="padding-right: 0">
        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'label' => 'دسته بندی',
                'format' => 'raw',
                'value' => function($data){
                    return $data->category->name;
                },
            ],
            'model',
            'series',
            [
                'label' => 'توضیحات',
                'format' => 'raw',
                'value' => function($data){
                    return $data->description;
                },
            ],
            [
                'label' => 'گارانتی',
                'format' => 'raw',
                'value' => function($data){
                    return $data->guaranty. ' سال';
                },
            ],
//            [
//                'label' => 'در دسترس',
//                'format' => 'raw',
//                'value' => function($data){
//                    $html = '<i class="fa fa-times" style="color: #F00;font-size:20px"></i>';
//                    if($data->available)
//                    {
//                        $html = '<i class="fa fa-check" style="color: #0F0;font-size:20px"></i>';
//                    }
//                    return $html;
//                },
//            ],

        ],
    ]) ?>
    </div>
    <h2 style="font-size: 18px; margin-bottom: 20px">تصاویر محصول</h2>
    <div style="display: flex;flex-flow: row; flex-wrap: wrap;  height: auto;width: 50%; background-color: white;" class="col-md-6">
        <?
        foreach ($model->productImages as $item){ ?>
            <a target="_blank" href="/images/uploads/product/<?=$item->name?>" style="background-color: #0A568C;width: 120px;height: 120px; margin: 4px;">
                <img src="/images/uploads/product/<?=$item->name?>" style="width: 100%; height: 100%;" alt="">
            </a>
        <?}?>

    </div>
    <h3 class="col-md-12" style="font-size:20px;margin-bottom: 10px;color: #1c2d3f;">ویژگی ها</h3>
    <table class="table table-striped col-md-6" style="margin-left: 1px;">
        <tbody>
        <?foreach ($model->category->propertyParents as $prop_parent){?>
            <tr>
                <td style="background-color: #2d2d2d; color: white;"><?=$prop_parent->name?></td>
                <td style="background-color: #2d2d2d; color: white;"></td>
            </tr>
            <?foreach ($prop_parent->categoryProps as $item) {?>
                <tr>
                    <td><?= $item->name ?></td>
                    <td><?= $item->getProductProps()->where(['product_id'=>$model->id])->one()['value'] ?></td>
                </tr>
            <?}
        }?>
        </tbody>
    </table>
    <div style="clear: both;"></div>

</div>
