<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\image_notice\models\ImageNotice */

$this->title = 'ویرایش اطلاعیه:'.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Image Notices', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="image-notice-update">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
