<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\image_notice\models\ImageNotice */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="image-notice-form">

    <?php $form = ActiveForm::begin(); ?>

    <?if($model->image){?>
        <img width="300" src="<?='/images/uploads/image-notice/'.$model->image?>" >
    <?}?>

    <?= $form->field($model, 'image', ['options' => ['style' => 'direction: rtl; text-align:right; margin: 20px 0;']])->fileInput(['rows' => 6]) ?>


    <?= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('ذخیره', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
