<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\image_notice\models\ImageNotice */

$this->title = 'ایجاد اطلاعیه';
$this->params['breadcrumbs'][] = ['label' => 'Image Notices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="image-notice-create">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
