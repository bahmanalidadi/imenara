<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\image_notice\models\ImageNotice */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Image Notices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="image-notice-view">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->

    <p>
        <?= Html::a('ویرایش', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('حذف', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label' => 'تصویر',
                'format' => 'raw',
                'value' => function($data){
                    return '<img width="300" src="/images/uploads/image-notice/'.$data->image.'" >';
                },
            ],
            'created_at',
        ],
    ]) ?>

</div>
