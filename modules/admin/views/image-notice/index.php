<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\image_notice\models\ImageNoticeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'اطلاع رسانی';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="image-notice-index">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('ایجاد اطلاعیه', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout'=>"{items}\n{pager}",
//        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'class' => 'yii\grid\DataColumn',
                'label' =>'تصویر',
                'format' => 'raw',
                'value' => function ($data) {
                    return '<img width="100" src="/images/uploads/image-notice/'.$data->image.'" >';
                },
            ],
            'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
