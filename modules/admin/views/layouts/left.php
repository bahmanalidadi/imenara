<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>مدیریت</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
<!--        <form action="#" method="get" class="sidebar-form">-->
<!--            <div class="input-group">-->
<!--                <input type="text" name="q" class="form-control" placeholder="Search..."/>-->
<!--              <span class="input-group-btn">-->
<!--                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>-->
<!--                </button>-->
<!--              </span>-->
<!--            </div>-->
<!--        </form>-->
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'پنل مدیریت وب سایت', 'options' => ['class' => 'header']],
                    [
                        'label' => 'مدیریت اخبار',
                        'icon' => 'newspaper-o',
                        'url' => '#',
                        'items' => [
                            ['label' => 'دسته بندی ها', 'icon' => 'th-large', 'url' => ['/admin/news-category']],
                            ['label' => 'خبرها', 'icon' => 'newspaper-o', 'url' => ['/admin/news']],
                        ],
                    ],
                    [
                        'label' => 'مدیریت محصول',
                        'icon' => 'newspaper-o',
                        'url' => '#',
                        'items' => [
                            ['label' => 'دسته بندی ها', 'icon' => 'th-large', 'url' => ['/admin/product-category']],
                            ['label' => 'محصولات', 'icon' => 'newspaper-o', 'url' => ['/admin/product']],
                        ],
                    ],
                    ['label' => 'مدیریت مطالب', 'icon' => 'file-text-o', 'url' => ['/admin/blog']],
                    ['label' => 'مدیریت اسلایدر', 'icon' => 'picture-o', 'url' => ['/admin/slider']],
                    ['label' => 'مدیریت کاتالوگ', 'icon' => 'book', 'url' => ['/admin/catalog']],
                    ['label' => 'تیکت های کاربران', 'icon' => 'ticket', 'url' => ['/admin/ticketing']],
//                    ['label' => 'مدیریت منوها', 'icon' => 'bars', 'url' => ['/admin/menu']],
//                    ['label' => 'اطلاعیه های مهم', 'icon' => 'bullhorn', 'url' => ['/admin/notice']],
//                    ['label' => 'اطلاع رسانی', 'icon' => 'bell', 'url' => ['/admin/image-notice']],
                    ['label' => 'خبرنامه', 'icon' => 'envelope', 'url' => ['/admin/newsletter']],
//                    ['label' => 'نظر سنجی', 'icon' => 'commenting', 'url' => ['/admin/poll']],
//                    [
//                        'label' => 'Some tools',
//                        'icon' => 'share',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
//                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
//                            [
//                                'label' => 'Level One',
//                                'icon' => 'circle-o',
//                                'url' => '#',
//                                'items' => [
//                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
//                                    [
//                                        'label' => 'Level Two',
//                                        'icon' => 'circle-o',
//                                        'url' => '#',
//                                        'items' => [
//                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
//                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
//                                        ],
//                                    ],
//                                ],
//                            ],
//                        ],
//                    ],

                ],
            ]
        ) ?>

    </section>

</aside>
