<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\menu\models\SiteMenu */

$this->title = 'ویرایش منو: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Site Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="site-menu-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
