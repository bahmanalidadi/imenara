<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\menu\models\SiteMenu */

$this->title = 'ایجاد آیتم منو';
$this->params['breadcrumbs'][] = ['label' => 'Site Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-menu-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
