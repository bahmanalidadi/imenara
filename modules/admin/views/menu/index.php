<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\menu\models\search_models\SiteMenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'منوهای بالای سایت';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-menu-index">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('ایجاد آیتم منو', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout'=>"{items}\n{pager}",
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'title',
            'link',
            'priority',
            [
                'class' => 'yii\grid\DataColumn',
                'label' =>'سرگروه',
                'format' => 'raw',
                'value' => function ($data) {
                    $html = '';
                    if($data->parent_id)
                    {
                        $html = \app\modules\menu\models\SiteMenu::find()->where(['id' => $data->parent_id])->one()->title;
                    }
                    return $html;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'label' =>'در دسترس',
                'format' => 'raw',
                'value' => function ($data) {
                    $html = '<i class="fa fa-times" style="color: #F00;font-size:20px"></i>';
                    if($data->available)
                    {
                        $html = '<i class="fa fa-check" style="color: #0F0;font-size:20px"></i>';
                    }
                    return $html;
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
