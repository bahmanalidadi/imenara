<?php

use app\classes\UsableFunctions;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\ticketing\models\Ticket */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-view">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->

    <p>
        <?= Html::a('ویرایش', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('حذف', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label' => 'کاربر',
                'format' => 'raw',
                'value' => function($data){
                    return $data->user->username;
                },
            ],
            'title',
            'description',
            [
                'label' => 'تاریخ ایجاد',
                'format' => 'raw',
                'value' => function($data){
                    if($data->created_at){
                        return UsableFunctions::get_jalali_date($data->created_at);
                    }
                },
            ],
            [
                'label' => 'وضعیت',
                'format' => 'raw',
                'value' => function($data){
                    return \app\modules\ticketing\models\Ticket::TICKET_STATUS[$data->status];
                },
            ],
            [
                'label' => 'فایل ضمیمه',
                'format' => 'raw',
                'value' => function($data){
                    if($data->attached_file){
                        return '<a href="/images/uploads/ticketing/'.$data->attached_file.'" target="_blank">دانلود فایل</a>';
                    }
                },
            ],
//            'attached_file',
        ],
    ]) ?>

</div>
