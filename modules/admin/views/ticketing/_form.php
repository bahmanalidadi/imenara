<?php

use dektrium\user\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\ticketing\models\Ticket */
/* @var $form yii\widgets\ActiveForm */

//$parents = \app\modules\ticketing\models\Ticket::find()->where('parent_id IS NULL');
//if($model->id){
//    $parents = $parents->andWhere('id != :id', [':id' => $model->id]);
//}
//$parents = $parents->all();
?>

<div class="ticket-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->dropDownList(\yii\helpers\ArrayHelper::map(User::find()->all(), 'id', 'username')) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>

<!--    --><?//= $form->field($model, 'parent_id')->dropDownList(\yii\helpers\ArrayHelper::map($parents, 'id', 'title')) ?>

<!--    --><?//= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList(\app\modules\ticketing\models\Ticket::TICKET_STATUS) ?>


    <?if($model->attached_file){?>
        <a href="<?='/images/uploads/ticketing/'.$model->attached_file?>" target="_blank" > دانلود فایل ضمیمه</a>
    <?}?>

    <?= $form->field($model, 'attached_file', ['options' => ['style' => 'direction: rtl; text-align:right; margin: 20px 0;']])->fileInput(['rows' => 6]) ?>


    <div class="form-group">
        <?= Html::submitButton('ذخیره', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
