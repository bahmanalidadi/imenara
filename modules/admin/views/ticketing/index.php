<?php

use app\classes\UsableFunctions;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\ticketing\models\search_models\TicketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'مدیریت تیکت ها';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-index">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('ایجاد تیکت', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'layout'=>"{items}\n{pager}",
        'columns' => [

            'id',
            [
                'class' => 'yii\grid\DataColumn',
                'label' =>'کاربر',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->user->username;
                },
            ],
            'title',
            'description',
//            'parent_id',

            [
                'class' => 'yii\grid\DataColumn',
                'label' =>'تاریخ ایجاد',
                'format' => 'raw',
                'value' => function ($data) {
                    if($data->created_at){
                        return UsableFunctions::get_jalali_date($data->created_at);
                    }
                },
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'label' =>'وضعیت',
                'format' => 'raw',
                'value' => function ($data) {
                    return \app\modules\ticketing\models\Ticket::TICKET_STATUS[$data->id];
                },
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'label' =>'فایل ضمیمه',
                'format' => 'raw',
                'value' => function ($data) {
                    if($data->attached_file){
                        return '<a href="/images/uploads/ticketing/'.$data->attached_file.'" target="_blank">دانلود فایل</a>';
                    }
                    return '';
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
