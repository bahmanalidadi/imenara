<?php

use unclead\multipleinput\MultipleInput;
use unclead\multipleinput\TabularColumn;
use unclead\multipleinput\TabularInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\poll\models\Poll */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .btn-add{
        height: 32px !important;
        margin-right: 10px;
        line-height: 28px;
    }
</style>
<div class="poll-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'question')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'available')->checkbox() ?>

    <?= $form->field($model, 'pollItems')->widget(MultipleInput::className(), [
        'min' => 3,
        'max' => 6,
        'allowEmptyList' => true,
        'addButtonOptions' => [
            'class' => 'btn-xs btn-success btn-add',
            'label' => 'افزودن گزینه',
            'style' => 'height:20px;margin-right:20px;',

        ],
        'removeButtonOptions' => [
            'class' => 'btn-xs btn-danger btn-add',
            'label' => 'حذف  گزینه'
        ],
        'attributeOptions' => [
            'enableAjaxValidation' => false,
            'enableClientValidation' => false,
            'validateOnChange' => false,
            'validateOnSubmit' => false,
            'validateOnBlur' => false,
        ],
        'columns' => [
            [
                'name' => 'id',
                'type' => TabularColumn::TYPE_HIDDEN_INPUT
            ],
            [
                'name' => 'item',
                'type' => 'textinput',
                'title' => ' گزینه ها',
                'headerOptions' => [
                    'style' => 'width: 30%;',
                    'class' => 'day-css-class'
                ]
            ],

        ]
    ]) ?>


    <div class="form-group">
        <?= Html::submitButton('ذخیره', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
