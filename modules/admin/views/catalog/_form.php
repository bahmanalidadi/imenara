<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\catalog\models\Catalog */
/* @var $form yii\widgets\ActiveForm */

$host_name = 'http://'.str_replace('backend.','',$_SERVER['SERVER_NAME']);
?>

<div class="catalog-form">

    <?php $form = ActiveForm::begin(); ?>

<!--    --><?//= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?if($model->image){?>
        <img width="300" src="<?='/images/uploads/catalog/'.$model->image?>" >
    <?}?>

    <?= $form->field($model, 'image', ['options' => ['style' => 'direction: rtl; text-align:right; margin: 20px 0;']])->fileInput(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('ذخیره', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
