<?php

use app\classes\UsableFunctions;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\catalog\models\Catalog */

$this->title = 'کاتالوگ '.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Catalogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalog-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('ویرایش', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('حذف', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'label' => 'تصویر',
                'format' => 'raw',
                'value' => function($data){
                    return '<img width="300" src="/images/uploads/catalog/'.$data->image.'" >';
                },
            ],
            [
                'label' => 'تاریخ ایجاد',
                'format' => 'raw',
                'value' => function($data){
                    if($data->created_at){
                        return UsableFunctions::get_jalali_date($data->created_at);
                    }
                },
            ],
        ],
    ]) ?>

</div>
