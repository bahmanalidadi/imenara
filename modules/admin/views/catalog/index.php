<?php

use app\classes\UsableFunctions;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\catalog\models\search_models\CatalogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'مدیریت کاتالوگ ها';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalog-index">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('ایجاد کاتالوگ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'layout'=>"{items}\n{pager}",
        'columns' => [
            'id',
//            'name',
            [
                'class' => 'yii\grid\DataColumn',
                'label' =>'تصویر',
                'format' => 'raw',
                'value' => function ($data) {
                    return '<img width="100" src="/images/uploads/catalog/'.$data->image.'" style="float: none;margin: 0 auto;">';
                },
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'label' =>'تاریخ ایجاد',
                'format' => 'raw',
                'value' => function ($data) {
                    if($data->created_at){
                        return UsableFunctions::get_jalali_date($data->created_at);
                    }
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
