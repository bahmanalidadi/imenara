<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\model_search\SearchBlog */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'مطالب وب سایت';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-index">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('ایجاد مطلب جدید', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'layout'=>"{items}\n{pager}",
        'columns' => [
            'id',
            'title',
            [
                'class' => 'yii\grid\DataColumn',
                'label' =>'تصویر',
                'format' => 'raw',
                'value' => function ($data) {
                    return '<img width="100" src="/images/uploads/blog/'.$data->image.'" >';
                },
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'label' =>'تاریخ ایجاد',
                'format' => 'raw',
                'value' => function ($data) {
                    return '';
//                    return \common\classes\UsableFunctions::get_jalali_date($data->created_at);
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ]
    ]); ?>
</div>
