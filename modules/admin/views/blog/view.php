<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Blog */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Blogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('ویرایش', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('حذف', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            [
                'label' => 'متن مطلب',
                'format' => 'raw',
                'value' => function($data){
                    return strip_tags($data->description);
                },
            ],
            [
                'label' => 'تصویر',
                'format' => 'raw',
                'value' => function($data){
                    return '<img width="300" src="/images/uploads/blog/'.$data->image.'" >';
                },
            ],
            [
                'label' => 'تاریخ ایجاد',
                'format' => 'raw',
                'value' => function($data){
//                    return \common\classes\UsableFunctions::get_jalali_date($data->created_at);
                },
            ],
        ],
    ]) ?>

</div>
