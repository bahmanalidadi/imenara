<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\news\models\search_models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'اخبار';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('ایجاد خبر', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'layout'=>"{items}\n{pager}",
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'class' => 'yii\grid\DataColumn',
                'label' =>'دسته بندی',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->category->title;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'label' =>'تصویر',
                'format' => 'raw',
                'value' => function ($data) {
                    return '<img width="100" src="/images/uploads/news/'.$data->image.'" >';
                },
            ],
            'title',
            [
                'class' => 'yii\grid\DataColumn',
                'label' =>'توضیحات',
                'format' => 'raw',
                'value' => function ($data) {
                    $str = implode(' ', array_slice(explode(' ', $data->description), 0, 20));
                    $str .= (strlen($str) < strlen($data->description)) ? '...': '';
                    return $str;
                },
            ],
//            'created_at',
//            'views',
            [
                'class' => 'yii\grid\DataColumn',
                'label' =>'در دسترس',
                'format' => 'raw',
                'value' => function ($data) {
                    $html = '<i class="fa fa-times" style="color: #F00;font-size:20px"></i>';
                    if($data->available)
                    {
                        $html = '<i class="fa fa-check" style="color: #0F0;font-size:20px"></i>';
                    }
                    return $html;
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
