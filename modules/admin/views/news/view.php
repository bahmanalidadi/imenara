<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\news\models\News */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->

    <p>
        <?= Html::a('ویرایش', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('حذف', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label' => 'دسته بندی',
                'format' => 'raw',
                'value' => function($data){
                    return $data->category->title;
                },
            ],
            [
                'label' => 'تصویر',
                'format' => 'raw',
                'value' => function($data){
                    return '<img width="100" src="/images/uploads/news/'.$data->image.'" >';
                },
            ],
            'title',
            'description:ntext',
            'created_at',
            'views',
            [
                'label' => 'در دسترس',
                'format' => 'raw',
                'value' => function($data){
                    $html = '<i class="fa fa-times" style="color: #F00;font-size:20px"></i>';
                    if($data->available)
                    {
                        $html = '<i class="fa fa-check" style="color: #0F0;font-size:20px"></i>';
                    }
                    return $html;
                },
            ],
        ],
    ]) ?>

</div>
