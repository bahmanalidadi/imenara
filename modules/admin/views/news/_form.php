<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\news\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\modules\news\models\NewsCategory::find()->where('available = 1')->all(), 'id', 'title')) ?>

    <?if($model->image){?>
        <img width="300" src="<?='/images/uploads/news/'.$model->image?>" >
    <?}?>

    <?= $form->field($model, 'image', ['options' => ['style' => 'direction: rtl; text-align:right; margin: 20px 0;']])->fileInput(['rows' => 6]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php

    ?>

    <label for="">انتخاب تگ</label>
    <?= Select2::widget([
        'name' => 'tags',
//        'value' =>$selected_tag_id,
//        'initValueText' => 'saasf',
        'data' => $tags,
        'toggleAllSettings' =>[
            'selectLabel' => '',
            'unselectLabel' => '',
        ],
        'options' => ['multiple' => true,'dir' => 'rtl' ,'placeholder' => 'انتخاب تگ ...'],
        'pluginOptions' => [
            'tags' => true,
            'tokenSeparators' => [',', ' '],
        ],
    ]); ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'available')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('ذخیره', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea' });</script>