<?php

namespace app\modules\shop\controllers;

use app\modules\product\models\Product;
use yii\web\Controller;

/**
 * Default controller for the `shop` module
 */
class ProductController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($id)
    {
        $product = Product::find()->where(['id' => $id])->one();
        $this->layout = '/page';
        return $this->render('index',[
            'product' => $product
        ]);
    }
}
