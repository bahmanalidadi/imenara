<?php

namespace app\modules\shop\controllers;

use app\modules\product\models\Product;
use yii\data\Pagination;
use yii\web\Controller;

/**
 * Default controller for the `shop` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $query = Product::find();
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $products = $query->offset($pages->offset)
            ->limit(20)
            ->all();

        $this->layout = '/page';
        return $this->render('index',[
            'products' => $products,
            'pages' => $pages,
        ]);
    }
    public function actionProducts($id)
    {
        die('here');
        $product = Product::find()->where(['id' => $id])->one();
        return $this->render('product-page',[
            'product' => $product
        ]);
    }
}
