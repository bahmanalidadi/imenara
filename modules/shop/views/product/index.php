<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\product\models\Product */
/* @var $form yii\widgets\ActiveForm */
$parents = \app\modules\product\models\ParentCategoryProp::find()->all();
$cats = \app\modules\product\models\ProductCategory::find()->orderBy('id ASC')->all();
$cat_props = \app\modules\product\models\CategoryProp::find()->where(['category_id' =>$cats[0]->id])->all();
?>
<style>
    .pager-active { border: 2px solid #5280DD; }
    .standard-icons > div {
        width: 14%;
    }
    .standard-icons img {
        padding: 0 5px;
    }
    .p_heading {
        background-color: #6aaac6;
        color: #fff;
        padding: 2px 7px;
        margin-bottom: 10px;
    }
    .list_item_desc {
        width: 80%;
        font-size: 12px;
        border-bottom: 1px solid #ccc;
    }
    .desc_item {
        color: #888;
        padding-top: 15px;
        width: 80%;
        font-size: 13px;
    }
</style>
<body style="background-color: #ECEDF1;">

    <div class="container" style="position:relative;border-radius:6px; box-shadow: 0px 3px 25px 0px rgba(32, 42, 54, 0.47);background-color: white;padding: 0px;">

        <div class="col-md-4 col-sm-4 col-xs-6" style="display: flex; flex-flow: column; 5px;border-radius: 5px;margin-top: 50px;">
            <div class="col-md-10" style="height: 350px; width: 100%; padding:5px;">
                <?if($product->productImages){?>
                    <img id="product_image_single" src="/images/uploads/product/<?=$product->productImages[0]->name?>" style="max-width: 300px;width: 100%;float: none;margin: 20px auto 0;" alt="">
                <?}?>
            </div>
            <div class="col-md-10" style="height: 90px;width: 100%;padding: 5px;text-align:center;border-radius: 5px;">
                <?php
                foreach ($product->productImages as $item){ ?>
                    <a style="cursor: pointer;width: 80px;height: 80px;display: inline-block;margin: 0 5px;" class="product_image_link">
                        <img src="/images/uploads/product/<?=$item->name?>" style="width: 100%; height: 100%;" alt="">
                    </a>
                <?}?>
            </div>
        </div>
        <div class="col-md-8" style="line-height: 25px;padding: 0 10px 0px 0;">
            <div class="col-md-12 no-padding" style="height: 250px;">
                <div class="col-md-7 no-padding" style="line-height:35px;font-size: 20px;padding-top: 100px !important;">
                    <b class="col-md-12" style="font-size: 35px;background-color: #6aaac6;color: #fff;display: inline-block;padding: 10px;margin-bottom: 20px;height: 45px;"><?=$product->model?></b><br>
                    <span><?=$product->name?></span> <br>
                    <div style=""> دسته بندی :
                        <span style="color: #499abb"> <?=$product->category->name?> </span>
                    </div>
                </div>
                <div class="col-md-5 no-padding" style="background-color: #6aaac6; padding-top: 100px !important;height: 100%;">
                    <b class="col-md-10" style="font-size: 30px;text-align: left;line-height: 34px;background-color: #fff;color: #6aaac6;display: inline-block;padding: 10px;height: 45px;">E-SERIES</b><br>
                    <span class="col-md-10" style="font-size: 25px;text-align: left;color: #fff;display: inline-block;padding: 10px;height: 45px;">سری <?=$product->series?></span><br>
                </div>
            </div>
            <div class="col-md-7 no-padding">
                <div class="col-md-12 no-padding" style="padding-top: 20px;min-height: 300px;"> <?=$product->description?> </div>
            </div>
            <div class="col-md-5" style="padding-top: 50px;min-height: 300px;background-color: #6aaac6">
                <img class="col-md-12" src="/images/logo_1.jpg" alt="">
                <img class="col-md-12" src="/images/logo_2.jpg" alt="">
                <img class="col-md-12" src="/images/logo_3.jpg" alt="">
            </div>
        </div>



<!--            <div class="standard-icons col-md-12" style="margin-top: 20px">-->
<!--                <span class="col-md-2" style="display: inline-block;">-->
<!--                    <img class="col-md-12" src="/images/gurantee_--><?//=$product->guaranty?><!--.png" alt="">-->
<!--                </span>-->
<!--                <span class="col-md-8" style="display: inline-block;">-->
<!--                    <div class="col-md-2 no-padding">-->
<!--                        <img src="/images/IMENARA CE.jpg" alt="" class="col-md-12">-->
<!--                    </div>-->
<!--                    <div class="col-md-2 no-padding">-->
<!--                        <img src="/images/IMENARA FCC.jpg" alt="" class="col-md-12">-->
<!--                    </div>-->
<!--                    <div class="col-md-2 no-padding">-->
<!--                        <img src="/images/IMENARA ISO.jpg" alt="" class="col-md-12">-->
<!--                    </div>-->
<!--                    <div class="col-md-2 no-padding">-->
<!--                        <img src="/images/IMENARA ISO10002.jpg" alt="" class="col-md-12">-->
<!--                    </div>-->
<!--                    <div class="col-md-2 no-padding">-->
<!--                        <img src="/images/IMENARA ISO10004.jpg" alt="" class="col-md-12">-->
<!--                    </div>-->
<!--                    <div class="col-md-2 no-padding">-->
<!--                        <img src="/images/IMENARA RoHs.jpg" alt="" class="col-md-12">-->
<!--                    </div>-->
<!--                </span>-->
<!--                <span class="col-md-2 no-padding">-->
<!--                    <div class="col-md-7 no-padding">-->
<!--                        <img src="/images/logo onvif.jpg" alt="" class="col-md-12 no-padding">-->
<!--                    </div>-->
<!--                </span>-->
<!--            </div>-->
    </div>


    <div class="container" style="border-radius:6px; background-color: white;margin-top: 20px;padding: 10px;box-shadow: 0px 3px 25px 0px rgba(32, 42, 54, 0.47);">
        <div class="product-form">

            <h3 class="col-md-12" style="font-size:20px;margin-bottom: 10px;color: #1c2d3f;margin-top: 10px;">مشخصات فنی</h3>
            <div class="col-md-12" style="margin-bottom: 20px"><?=$product->category->name.' '.$product->name?></div>
            <table class="table table-striped col-md-12" style="margin-left: 1px;">
                <tbody>
                <?foreach ($product->category->propertyParents as $prop_parent){?>
                    <tr>
                        <td style="background-color: #6aaac6; color: white;"><?=$prop_parent->name?></td>
                        <td style="background-color: #6aaac6; color: white;"></td>
                    </tr>
                    <?foreach ($prop_parent->categoryProps as $item) {?>
                        <tr>
                            <td style="background-color: #f0f1f2; width: 30%"><?= $item->name ?></td>
                            <td dir="auto" style="text-align: right;background-color: #f7f9fa; width: 70%"><?= $item->getProductProps()->where(['product_id'=>$product->id])->one()['value'] ?></td>
                        </tr>
                    <?}
                }?>
                </tbody>
            </table>


            <div style="clear: both;"></div>
            <input type="hidden" >

        </div>
    </div>


<?php
$this->registerJs(<<<JS
    $('.product_image_link img').click(function() {
        var source = $(this).attr('src');
        $('#product_image_single').fadeOut(200);
        setTimeout(function(){
            $('#product_image_single').attr('src', source);
            $('#product_image_single').fadeIn(200);
        },200);
    });
JS
        , \yii\web\View::POS_READY);
    ?>
</body>