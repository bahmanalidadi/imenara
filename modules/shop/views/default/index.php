<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $model app\modules\product\models\Product */
/* @var $form yii\widgets\ActiveForm */
$parents = \app\modules\product\models\ParentCategoryProp::find()->all();
$cats = \app\modules\product\models\ProductCategory::find()->orderBy('id ASC')->all();
$cat_props = \app\modules\product\models\CategoryProp::find()->where(['category_id' =>$cats[0]->id])->all();
?>

<body style="background-color: #ECEDF1;">
<div class="product-form">
    <div class="container  no-padding">
        <div class="col-md-12 no-padding">
            <?foreach ($products as $product){?>
                <a  class="col-md-3" style="padding: 15px; height: 420px;text-align: center; display: flex;flex-flow: column;border-radius: 5px;" href="/shop/product?id=<?=$product->id?>">
                    <div class="col-md-12 no-padding" style="box-shadow: 0px 3px 25px 0px rgba(32, 42, 54, 0.47);height: 380px;border-radius: 6px;background-color: #fff">
                        <div class="" >
                            <div style="width: 100%; height: 250px; margin: 0 auto">
                                <?if($product->productImages){?>
                                    <img src="/images/uploads/product/<?=$product->productImages[0]->name?>" style="padding: 30px;width: 100%;border-radius:6px 6px 0 0;width: 100%;" alt="">
                                <?}else{?>
                                    <img src="/images/prod.png?>" style="width: 100%;" alt="">
                                <?}?>
                            </div>
                            <?if($product->series){?>
                                <div style="font-size: 16px;margin-top: 10px;color: #0c91e5">سری <?=$product->series?></div>
                            <?}?>
                            <div style="font-size: 16px; margin-top: 10px;line-height: 23px;"><?=$product->name?></div>
                            <?if($product->model){?>
                                <div style="font-size: 12px;margin-top: 10px;color: #0c91e5"><?=$product->model?></div>
                            <?}?>
                        </div>
                    </div>
                </a>
            <?}?>
        </div>

        <div class="col-md-12" style="text-align: center">
            <?php
            echo LinkPager::widget([
                'pagination' => $pages,
            ]);
            ?>
        </div>

        <div style="clear: both;"></div>
        <input type="hidden" >
    </div>
</div>

</body>
