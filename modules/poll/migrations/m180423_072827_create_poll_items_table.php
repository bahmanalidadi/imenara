<?php

use yii\db\Migration;

/**
 * Handles the creation of table `poll_items`.
 */
class m180423_072827_create_poll_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('poll_item', [
            'id' => $this->primaryKey(),
            'item' => $this->string(),
            'poll_id' =>$this->integer(),
        ]);
        $this->createIndex('idx-question_item_id',
            'poll_item',
        'poll_id'
            );
        $this->addForeignKey('fk-question_item_id',
            'poll_item',
        'poll_id',
        'poll',
        'id',
        'CASCADE',
        'CASCADE'
            );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-question_item_id','poll_item');
        $this->dropIndex('idx-question_item_id','poll_item');
        $this->dropTable('poll_item');
    }
}
