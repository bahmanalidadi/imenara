<?php

use yii\db\Migration;

/**
 * Handles the creation of table `poll`.
 */
class m180423_072404_create_poll_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('poll', [
            'id' => $this->primaryKey(),
            'question' => $this->string(),
            'available' => $this->boolean(),
            'created_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('poll');
    }
}
