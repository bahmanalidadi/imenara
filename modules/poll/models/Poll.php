<?php

namespace app\modules\poll\models;

use Yii;

/**
 * This is the model class for table "poll".
 *
 * @property int $id
 * @property string $question
 * @property int $available
 * @property int $created_at
 *
 * @property PollItem[] $pollItems
 */
class Poll extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'poll';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at'], 'integer'],
            [['question'], 'string', 'max' => 255],
            [['available'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'شماره',
            'question' => 'سوال',
            'available' => 'در دسترس',
            'created_at' => 'تاریخ ایجاد',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollItems()
    {
        return $this->hasMany(PollItem::className(), ['poll_id' => 'id']);
    }
}
