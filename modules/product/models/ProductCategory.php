<?php

namespace app\modules\product\models;

use Yii;

/**
 * This is the model class for table "product_category".
 *
 * @property int $id
 * @property string $name
 * @property string $created_at
 *
 * @property CategoryProp[] $categoryProps
 * @property Product[] $products
 */
class ProductCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'created_at'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'شماره',
            'name' => 'نام',
            'created_at' => 'تاریخ ایجاد',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryProps()
    {
        return $this->hasMany(CategoryProp::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }
    public function getPropertyParents()
    {
        $parents = ParentCategoryProp::find()
            ->innerJoin('category_prop','category_prop.parent_id = parent_category_prop.id')
            ->innerJoin('product_category' ,'product_category.id = category_prop.category_id')
            ->where(['product_category.id'=>$this->id])->all();
        return $parents;
    }
}
