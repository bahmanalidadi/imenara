<?php

namespace app\modules\product\models;

use Yii;

/**
 * This is the model class for table "product_prop".
 *
 * @property int $id
 * @property string $value
 * @property int $product_id
 * @property int $category_prop_id
 *
 * @property CategoryProp $categoryProp
 * @property Product $product
 */
class ProductProp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_prop';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'category_prop_id'], 'integer'],
            [['value'], 'string', 'max' => 255],
            [['category_prop_id'], 'exist', 'skipOnError' => true, 'targetClass' => CategoryProp::className(), 'targetAttribute' => ['category_prop_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'Value',
            'product_id' => 'Product ID',
            'category_prop_id' => 'Category Prop ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryProp()
    {
        return $this->hasOne(CategoryProp::className(), ['id' => 'category_prop_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
