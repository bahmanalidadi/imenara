<?php

namespace app\modules\product\models;

use Yii;

/**
 * This is the model class for table "category_prop".
 *
 * @property int $id
 * @property string $name
 * @property int $category_id
 *
 * @property ProductCategory $category
 * @property ProductProp[] $productProps
 */
class CategoryProp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category_prop';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'category_id' => 'Category ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ProductCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductProps()
    {
        return $this->hasMany(ProductProp::className(), ['category_prop_id' => 'id']);
    }

    public function getParent()
    {
        return $this->hasOne(ParentCategoryProp::className(), ['id' => 'parent_id']);
    }
}
