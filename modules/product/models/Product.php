<?php

namespace app\modules\product\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $name
 * @property string $created_at
 * @property int $available
 * @property int $category_id
 *
 * @property ProductCategory $category
 * @property ProductProp[] $productProps
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'guaranty'], 'integer'],
            [['iso_9001','iso_10004','iso_10002','CE','FC','RoHS','Onvif'], 'boolean'],
            [['name', 'created_at', 'model', 'series'], 'string', 'max' => 255],
            [['description'], 'string'],
            [['available'], 'string', 'max' => 1],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'کد محصول',
            'name' => 'نام محصول',
            'created_at' => 'تاریخ ایجاد',
            'available' => 'در دسترس',
            'category_id' => 'دسته بندی',
            'description' => 'توضیحات',
            'model' => 'مدل',
            'guaranty' => 'گارانتی (سال)',
            'series' => 'سری',
            'iso_9001' => '',
            'iso_10004' => '',
            'iso_10002' => '',
            'CE' => '',
            'FC' => '',
            'RoHS' => '',
            'Onvif' => ''
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ProductCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductProps()
    {
        return $this->hasMany(ProductProp::className(), ['product_id' => 'id']);
    }

    public function getProductImages()
    {
        return $this->hasMany(ProductImage::className(), ['product_id' =>'id']);
    }

    public function get_prop_value($cat_prop_id)
    {
        $prop_value =  $this->getProductProps()->where(['category_prop_id' => $cat_prop_id])->one();
        return $prop_value ? $prop_value->value : '';
    }
}
