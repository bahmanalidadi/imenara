<?php

namespace app\modules\product\models;

use Yii;

/**
 * This is the model class for table "parent_category_prop".
 *
 * @property int $id
 * @property string $name
 *
 * @property CategoryProp[] $categoryProps
 */
class ParentCategoryProp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parent_category_prop';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryProps()
    {
        return $this->hasMany(CategoryProp::className(), ['parent_id' => 'id']);
    }
}
