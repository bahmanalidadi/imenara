<?php

use yii\db\Migration;

/**
 * Class m180521_071310_add_description_to_product_table
 */
class m180521_071310_add_description_to_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product','description','string');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180521_071310_add_description_to_product_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180521_071310_add_description_to_product_table cannot be reverted.\n";

        return false;
    }
    */
}
