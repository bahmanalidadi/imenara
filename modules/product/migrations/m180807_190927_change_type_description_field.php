<?php

use yii\db\Migration;

/**
 * Class m180807_190927_change_type_description_field
 */
class m180807_190927_change_type_description_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('product', 'description', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180807_190927_change_type_description_field cannot be reverted.\n";

        return false;
    }
    */
}
