<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_prop`.
 */
class m180517_065150_create_product_prop_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_prop', [
            'id' => $this->primaryKey(),
            'value' => $this->string(),
            'product_id' =>$this->integer(),
            'category_prop_id' =>$this->integer(),
        ]);
        $this->createIndex('idx-product_prop_product','product_prop','product_id');
        $this->addForeignKey('fk-product_prop_product',
            'product_prop',
            'product_id',
            'product',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->createIndex('idx-product_prop_category_prop','product_prop','category_prop_id');
        $this->addForeignKey('fk-product_prop_category_prop',
            'product_prop',
            'category_prop_id',
            'category_prop',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-product_prop_category_prop','product_prop');
        $this->dropIndex('idx-product_prop_category_prop','product_prop');
        $this->dropForeignKey('fk-product_prop_product','product_prop');
        $this->dropIndex('idx-product_prop_product','product_prop');
        $this->dropTable('product_prop');
    }
}
