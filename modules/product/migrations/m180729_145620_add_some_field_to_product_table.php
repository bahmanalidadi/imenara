<?php

use yii\db\Migration;

/**
 * Class m180729_145620_add_some_field_to_product_table
 */
class m180729_145620_add_some_field_to_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product','model', $this->string());
        $this->addColumn('product','guaranty', $this->integer());
        $this->addColumn('product','iso_9001', $this->boolean());
        $this->addColumn('product','iso_10004', $this->boolean());
        $this->addColumn('product','iso_10002', $this->boolean());
        $this->addColumn('product','CE', $this->boolean());
        $this->addColumn('product','FC', $this->boolean());
        $this->addColumn('product','RoHS', $this->boolean());
        $this->addColumn('product','Onvif', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product', 'model');
        $this->dropColumn('product', 'guaranty');
        $this->dropColumn('product','iso_9001');
        $this->dropColumn('product','iso_10004');
        $this->dropColumn('product','iso_10002');
        $this->dropColumn('product','CE');
        $this->dropColumn('product','FC');
        $this->dropColumn('product','RoHS');
        $this->dropColumn('product','Onvif');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180729_145620_add_some_field_to_product_table cannot be reverted.\n";

        return false;
    }
    */
}
