<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 */
class m180517_063959_create_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'created_at' => $this->string(),
            'available' => $this->boolean(),
            'category_id' => $this->integer()
        ]);
        $this->createIndex('idx-product_category','product','category_id');
        $this->addForeignKey('fk-product_category',
            'product',
            'category_id',
            'product_category',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-product_category','product');
        $this->dropIndex('idx-product_category','product');
        $this->dropTable('product');
    }
}
