<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category_prop`.
 */
class m180517_062350_create_category_prop_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('category_prop', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'category_id' => $this->integer(),
        ]);
        $this->createIndex('idx-category_prop_category','category_prop','category_id');
        $this->addForeignKey('fk-category_prop_category',
            'category_prop',
            'category_id',
            'product_category',
            'id',
            'CASCADE',
            'CASCADE'
            );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-category_prop_category','category_prop');
        $this->dropIndex('idx-category_prop_category','category_prop');
        $this->dropTable('category_prop');
    }
}
