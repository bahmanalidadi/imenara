<?php

use yii\db\Migration;

/**
 * Class m180807_195554_add_product_serie_field
 */
class m180807_195554_add_product_serie_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product', 'series', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180807_195554_add_product_serie_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180807_195554_add_product_serie_field cannot be reverted.\n";

        return false;
    }
    */
}
