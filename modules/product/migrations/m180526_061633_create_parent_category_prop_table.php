<?php

use yii\db\Migration;

/**
 * Handles the creation of table `parent_category_prop`.
 */
class m180526_061633_create_parent_category_prop_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('parent_category_prop', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
        $this->addColumn('category_prop','parent_id','integer');
        $this->createIndex('idx-parent_category_id','category_prop','parent_id');
        $this->addForeignKey('fk-parent_category_id',
            'category_prop',
            'parent_id',
            'parent_category_prop',
            'id',
            'CASCADE',
            'CASCADE'
            );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-parent_category_id','category_prop');
        $this->dropIndex('idx-parent_category_id','category_prop');
        $this->dropTable('parent_category_prop');
    }
}
