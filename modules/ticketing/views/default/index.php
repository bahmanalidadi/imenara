<style>
    td, th{
        text-align: center;
    }
</style>
<div class="container">
    <h1 style="font-size: 25px; margin: 50px 0 50px;">درخواست های پشتیبانی ارسال شده</h1>
    <table class="table table-striped">
        <thead>
            <tr>
                <td>شماره تیکت</td>
                <td>عنوان</td>
                <td>متن درخواست</td>
                <td>تاریخ ایجاد</td>
                <td>وضعیت</td>
                <td></td>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($tickets as $ticket) {?>
                <tr>
                    <td>#<?=$ticket->id?></td>
                    <td><?=$ticket->title?></td>
                    <td data-toggle="tooltip" title="<?=$ticket->description?>"><?=\app\classes\UsableFunctions::substr_by_words_length($ticket->description,15)?></td>
                    <td><?=\app\classes\UsableFunctions::get_jalali_date($ticket->created_at, 'Y-m-d')?></td>
                    <td><?=\app\modules\ticketing\models\Ticket::TICKET_STATUS[$ticket->status]?></td>
                    <td><a href="/ticketing/default/show?id=<?=$ticket->id?>" style="color: #499abb;">نمایش</a></td>
                </tr>
            <?}?>
        </tbody>
    </table>
</div>

<?php
$this->registerJs(<<<JS
        $('[data-toggle="tooltip"]').tooltip();
JS
, \yii\web\View::POS_READY)
?>