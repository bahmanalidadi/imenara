<?php

namespace app\modules\ticketing\controllers;

use app\modules\ticketing\models\Ticket;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Default controller for the `ticketing` module
 */
class DefaultController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $tickets = Ticket::find()->where(['user_id' => \Yii::$app->user->id])->andWhere('parent_id IS NULL')->all();
        $this->layout = '/page';
        return $this->render('index', ['tickets' => $tickets]);
    }

    public function actionShow()
    {
        die("here");
        $id = \Yii::$app->request->get('id');
        $ticket = Ticket::find()->where(['user_id' => \Yii::$app->user->id, 'id' => $id])->one();
        $this->layout = '/page';
        return $this->render('show', ['ticket' => $ticket]);
    }
}
