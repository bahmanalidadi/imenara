<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ticket`.
 */
class m180604_073856_create_ticket_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('ticket', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'title' => $this->string(),
            'description' => $this->string(),
            'parent_id' => $this->integer(),
            'created_at' => $this->integer(),
            'status' => $this->integer(),
            'attached_file' => $this->string(),
        ]);

        $this->createIndex('idx-ticket_user_id', 'ticket', 'user_id');
        $this->addForeignKey(
            'fk-ticket_user_id',
            'ticket',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-ticket_user_id', 'ticket');
        $this->dropIndex('idx-ticket_user_id', 'ticket');
        $this->dropTable('ticket');
    }
}
