<?php

namespace app\modules\menu\models;

use Yii;

/**
 * This is the model class for table "site_menu".
 *
 * @property integer $id
 * @property string $title
 * @property string $link
 * @property integer $priority
 * @property integer $parent_id
 * @property integer $available
 */
class SiteMenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['priority', 'parent_id', 'available'], 'integer'],
            [['title', 'link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'شماره',
            'title' => 'عنوان',
            'link' => 'لینک',
            'priority' => 'اولویت',
            'parent_id' => 'دسته بالایی',
            'available' => 'در دسترس',
        ];
    }
}
