<?php

use yii\db\Migration;

/**
 * Handles the creation of table `menu`.
 */
class m180327_070616_create_menu_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('site_menu', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'link' => $this->string(),
            'priority' => $this->integer(),
            'parent_id' => $this->integer()->defaultValue(0),
            'available' => $this->boolean(),

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('site_menu');
    }
}
