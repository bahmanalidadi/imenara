<?php

/* @var $this yii\web\View */
$this->title = 'شرکت ایمن آرا';
?>


    <link rel="stylesheet" href="/css/index.css">
<!--    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">-->


    <div class="no-padding container-fluid">
        <div class="">
            <img src="/images/bac.png" style="width:100%;background-image: url('/images/uploads/slider/<?=$sliders[0]->image?>')" alt="">

            <nav class="navbar menu-bar nav-top navbar-default container" style="border: none !important;width: 90%;position: absolute;right: 5%;">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#">
                            <div class="col-md-2 item logo-fa"></div>
                        </a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand logo" href="#" style="padding-top: 10px;color: #ffffff;"><img
                                    style="float: left;margin-top: 0" src="/images/top_logo.png" alt=""></a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-1"
                         style="background-color: #ffffff;">
                        <ul class="navbar-nav menu-bar top-bar"
                            style="background-color:white;z-index : 102;list-style: none;height: auto;position: relative; display: flex;">
                            <a href="/" style="padding: 22px 15px; text-decoration: none;  font-weight: bolder;">
                                <li class="nav-item" style="">خانه</li>
                            </a>
                            <a href="/shop" style="padding: 22px 15px; text-decoration: none;font-weight: bolder;">
                                <li class="nav-item" style="">محصولات</li>
                            </a>
                            <a href="" style="padding: 22px 15px; text-decoration: none; font-weight: bolder;">
                                <li class="nav-item" style="">جذب کارشناس</li>
                            </a>
                            <a href="" style="padding: 22px 15px; text-decoration: none; font-weight: bolder;">
                                <li class="nav-item" style="">آموزش</li>
                            </a>
                            <a href="" style="padding: 22px 15px; text-decoration: none; font-weight: bolder;">
                                <li class="nav-item" style="">تماس با ما</li>
                            </a>
                            <? if (Yii::$app->user->isGuest) { ?>
                                <a href="/user/login"
                                   style="padding: 22px 15px; text-decoration: none; color: #202a36; font-weight: bolder;">
                                    <li class="nav-item" style="">ورود</li>
                                </a>
                                <a href="/user/register"
                                   style="padding: 22px 15px; text-decoration: none; color: #202a36; font-weight: bolder;">
                                    <li class="nav-item" style="">ثبت نام</li>
                                </a>
                            <? } else { ?>
                                <a href="/user/logout" data-method="post"
                                   style="padding: 22px 15px; text-decoration: none; color: #202a36; font-weight: bolder;">
                                    <li class="nav-item" style="">خروج</li>
                                </a>
                            <? } ?>
                        </ul>
                        <!--            <img style="float: left;margin-top: 10px;" src="/images/top_logo.png" alt="">-->
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>

        </div>
        <div class="container navigate">
            <a class="x col-md-2 col-xs-4 col-sm-4" style="display: flex;flex-flow: column;">
                <div class="item item-1 col-md-2">
                    <div class="img-nav img-nav0" style="top: 39px;left: -29px;"></div>
                </div>
                <p class="title-nav" style="top : -97px;">دوربین آنالوگ</p>
            </a>
            <a class="x col-md-2 col-xs-4 col-sm-4" style="display: flex;flex-flow: column;">
                <div class="item item-2 col-md-2">
                    <div class="img-nav img-nav1" style="top: 51px;left: -32px;"></div>
                </div>
                <p class="title-nav" style="top: -54px;">دوربین آنالوگ</p>
            </a>
            <a class="x col-md-2 col-xs-4 col-sm-4" style="display: flex;flex-flow: column;">
                <div class="item item-3 col-md-2">
                    <div class="img-nav2 img-nav" style="top: 39px;left: -29px;"></div>
                </div>
                <p class="title-nav" style="top: -31px;">دوربین آنالوگ</p>
            </a>
            <a class="x col-md-2 col-xs-4 col-sm-4" style="display: flex;flex-flow: column;">
                <div class="item item-4 col-md-2">
                    <div class="img-nav3 img-nav" style="top: 48px;left: -21px;"></div>
                </div>
                <p class="title-nav" style="top: -31px;">دوربین آنالوگ</p>
            </a>
            <a class="x col-md-2 col-xs-4 col-sm-4" style="display: flex;flex-flow: column;">
                <div class="item item-5 col-md-2">
                    <div class="img-nav img-nav4" style="top: 40px;left: -25px;"></div>
                </div>
                <p class="title-nav" style="top: -54px;">دوربین آنالوگ</p>
            </a>
            <a class="x col-md-2 col-xs-4 col-sm-4" style="display: flex;flex-flow: column;">
                <div class="item item-6 col-md-2">
                    <div class="img-nav5 img-nav" style="top: 40px;left: -22px;"></div>
                </div>
                <p class="title-nav" style="top : -97px;">دوربین آنالوگ</p>
            </a>

        </div>



        <div class="container latest-news " id="container11">
            <div class="title-news">جدیدترین اخبار</div>
            <div class="sub-title-news">جدید ترین اخبارهای گروه ایمن آرا</div>
            <div class="border"></div>
            <div class="demo-wrap">
                <div id="demo" class="demo">
                    <ul class="slide-wrap" id="example">
                        <?php
                        $i=1;
                        foreach ($latest_news as $news) {?>
                            <li class="pos<?=$i?>">
                                <a href="/news/default/show?id=<?=$news->id?>">
                                    <div class="item-news">
                                        <img src="/images/uploads/news/<?=$news->image?>" style="border-radius: 6px 6px 0 0;height: 220px;width: auto;background-color: #0A568C;" alt="">
                                        <div class="title-item-news">
                                            <?=$news->title?>
                                        </div>
                                        <div class="desc-item-news"><?=$news->get_short_description(45)?></div>
                                        <div class="details-item-news">
                                            <p class="date-comments-news col-md-12"
                                               style="color: #bab4b2;margin-top: 4%;text-align: left;">
                                                <span>
                                                    <?=\app\classes\UsableFunctions::get_jalali_date($news->created_at, 'd F y')?>
                                                    <i class="fa fa-clock-o"></i>
                                                </span>
                                                <span class="comment-news" style="padding-right: 10px;">
                                                    <?=$news->views?$news->views:0?>
                                                    <i class="fa fa-eye"></i>
                                                </span>
                                                <i class="fa fa-share-alt pull-right"></i>
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        <?$i++;}
                        ?>
                    </ul>
                    <i class="arrow prev" id="jprev">&lt;</i>
                    <i class="arrow next" id="jnext">&gt;</i>
                    <div class="low-opacity"></div>
                </div>
            </div>
            <!--aacasac-->
        </div>
    </div>

<?php
$this->registerJs(<<<JS
   $('#demo').RollingSlider({
            showArea:"#example",
            prev:"#jprev",
            next:"#jnext",
            moveSpeed:300,
            autoPlay:false
        });


    // $('#container11').carousel({
    //     num: 5,
    //     maxWidth: 450,
    //     maxHeight: 300,
    //     distance: 50,
    //     scale: 0.6,
    //     animationTime: 1000,
    //     showTime: 4000
    // });
    // $('#container2').carousel({
    //     num: 5,
    //     maxWidth: 200,
    //     maxHeight: 100,
    //     showTime: 5000
    // });
    //
    // $('#container3').carousel({
    //     num: 5,
    //     maxWidth: 300,
    //     maxHeight: 250,
    //     distance: 30,
    //     autoPlay: false
    // });
JS
    , \yii\web\View::POS_READY);
?>