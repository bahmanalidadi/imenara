<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
<!--    --><?php
//    NavBar::begin([
//        'brandLabel' => Yii::$app->name,
//        'brandUrl' => Yii::$app->homeUrl,
//        'options' => [
//            'class' => 'navbar-inverse navbar-fixed-top',
//        ],
//    ]);
//    echo Nav::widget([
//        'options' => ['class' => 'navbar-nav navbar-right'],
//        'items' => [
//            ['label' => 'Home', 'url' => ['/site/index']],
//            ['label' => 'About', 'url' => ['/site/about']],
//            ['label' => 'Contact', 'url' => ['/site/contact']],
//            Yii::$app->user->isGuest ? (
//                ['label' => 'Login', 'url' => ['/site/login']]
//            ) : (
//                '<li>'
//                . Html::beginForm(['/site/logout'], 'post')
//                . Html::submitButton(
//                    'Logout (' . Yii::$app->user->identity->username . ')',
//                    ['class' => 'btn btn-link logout']
//                )
//                . Html::endForm()
//                . '</li>'
//            )
//        ],
//    ]);
//    NavBar::end();
//    ?>
<!--    <div style="clear: both;"></div>-->
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <?= Alert::widget() ?>
    <?= $content ?>
</div>

<footer class="footer">
    <div class="" style="display: flex;flex-flow: column">
        <div class="phone-number col-md-5 col-sm-10 col-xs-10">
            <div class="col-md-6 col-xs-6 col-sm-6" style="padding-top: 16px; border-left: 1px solid #a4a4a4;display: flex;flex-flow: row">
                <div dir="ltr" style="font-size: 13px;color: #c1cad6;padding-top: 5px; margin: 0 auto;">(032)&nbsp;882-167-2000</div><img src="/images/phone icon.png" style="padding-right:13px;height: 20px;" alt="">
            </div>
            <div class="col-md-6 col-xs-6 col-sm-6" style="padding-top: 16px;display: flex;flex-flow: row">
                <img src="/images/headphone.png" style="padding-left:10px;height: 20px;" alt=""><div dir="ltr" style="font-size: 13px;margin: 0 auto;padding-top:5px;color: #c1cad6;">(032)&nbsp;882-167-2000</div>
            </div>
        </div>
<!--        <div class="container footer-nav  no-padding">-->
<!--            <div class="footer-nav-item col-md-1 col-sm-2 col-xs-4 no-padding"><a style="text-align: right" href=""> قوانین</a></div>-->
<!--            <div class="footer-nav-item col-md-2 col-sm-2 col-xs-4 no-padding"><a href=""> درباره ما</a></div>-->
<!--            <div class="footer-nav-item col-md-2 col-sm-2 col-xs-4 no-padding"><a href=""> رزومه</a></div>-->
<!--            <div class="footer-nav-item col-md-2 col-sm-2 col-xs-4 no-padding"><a href=""> تماس با ما</a></div>-->
<!--            <div class="footer-nav-item col-md-2 col-sm-2 col-xs-4 no-padding"><a href=""> شکایات</a></div>-->
<!--            <div class="footer-nav-item col-md-2 col-sm-2 col-xs-4 no-padding"><a href=""> شکایات</a></div>-->
<!--            <div class="footer-nav-item col-md-1 col-sm-2 col-xs-4 no-padding"><a style="text-align: left" href=""> پشتیبانی</a></div>-->
<!--        </div>-->
        <div class="container-fluid"style="display:flex;flex-flow:row wrap;border-bottom:  1px solid #d9dbe0;border-top:1px solid #d9dbe0; width: 100%;height: 80px;margin-top: 45px;">
            <div class="container no-padding">
                <div class="col-md-4 col-sm-12 col-xs-12 links" style="display: flex;flex-flow:row;border-left: 1px solid #d9dbe0;height: 100%;padding: 31px 0 0 0;;text-align: center; font-size: 20px;">
                    <div class="col-md-3 col-xs-4 cpl-sm-4 no-padding"><a href="" style="text-align: right"><i class="fa fa-youtube"></i></a></div>
                    <div class="col-md-4 col-xs-4 cpl-sm-4 no-padding"><a href=""><i class="fa fa-instagram"></i></a></div>
                    <div class="col-md-4 col-xs-4 cpl-sm-4 no-padding"><a href=""><i class="fa fa-facebook"></i></a></div>
                    <div class="col-md-5 col-xs-4 cpl-sm-4 no-padding"><a href=""><i class="fa fa-twitter"></i></a></div>
                </div>
                <div class="br-btm col-md-4 col-sm-6 col-xs-6" style="text-align: center;">
                    <input dir="rtl" style="text-align: center; padding-right: 0" id="email" class="form-control newspaper-input col-md-10 col-xs-10 col-sm-10 input" placeholder="عضویت در خبرنامه ایمن آرا" type="email">
                    <a href="">
                        <i class="fa fa-arrow-right" style="position: absolute;top: 32px;right:45px;"></i>
                    </a>
                    <i class="fa fa-envelope" style="position: absolute;top: 32px;left: 49px;"></i>
                </div>
                <div class="br-btm col-md-4 col-sm-6 col-xs-6 no-padding"style="height: 100%;border-right: 1px solid #d9dbe0;">
                    <img src="/images/layer 804.png" class="col-md-8" style="    float: left;padding: 7px 0 0 0; float: left" alt="">
                </div>
            </div>


        </div>
        <div class="sub-footer container" style="padding: 0px; display: flex; flex-flow: row; position: relative">
            <div class="col-md-9 col-xs-11 imen no-padding">کلیه حقوق مادی و معنوی برای شرکت <a href="/" style="margin: 0 8px;"> ایمن آرا </a> محفوظ است و هرگونه کپی برداری غیر مجاز پیگرد قانونی دارد</div>
            <div class="col-md-3" style="padding: 10px 0; float: left;">
                <span style="float:left;">
                    <img src="/images/Vector Smart Object7.png" alt="" style="width: 25px;margin-top: 3px;">
                </span>
                <div class=" imen" style="float: left; display: inline-block;" >طراحی&nbsp;و&nbsp;اجرا&nbsp;توسط&nbsp;تیم</div>
            </div>

        </div>

    </div>
</footer>
<?php
//$this->registerJs(<<<JS
//
//    $('#send').click(function () {
//        $.ajax({
//            url : "/site/send-email",
//            method : "post",
//            dataType : "json",
//            data : {
//                email : $('#email').val(),
//            },
//            success : function(data){
//                $('#email').val('');
//                alert(data);
//            }
//        });
//
//    });
//
//
//JS
//    , \yii\web\View::POS_READY)
//?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

