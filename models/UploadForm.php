<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;
    public $videoFile;
    public $jpgFile;
    public $multipleFiles;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, ico, bmp, pdf, txt, doc, docx, xls, xlsx'],
            [['jpgFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'jpg', 'jpeg'],
            [['videoFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'mp4, jpeg, mkv', 'maxSize' => 2048000, 'tooBig' => 'Limit is 2MB'],
            [['multipleFiles'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'maxFiles' => 4],

        ];
    }

    public function upload($url='images/uploads/blog/')
    {
        if($this->multipleFiles){
            $names = [];
            foreach ($this->multipleFiles as $file) {
                $name = Yii::$app->user->id .'-'.time().rand(1,1000000). '.' . $file->extension;
                $file->saveAs($url . $name);
                $names[] = $name;
            }
            return $names;
        }
        else{
            $name = Yii::$app->user->id .'-'.time(). '.' . $this->imageFile->extension;
            $this->imageFile->saveAs($url . $name);
            return $name;
        }

    }
    public function upload_video($url='images/uploads/projects/')
    {
        $name = Yii::$app->user->id .'-'.time(). '.' . $this->videoFile->extension;
        $this->videoFile->saveAs($url . $name);
        return $name;
    }

    public function upload_jpg($url='images/uploads/projects/')
    {

        $name = Yii::$app->user->id .'-'.time(). '.' . $this->jpgFile->extension;
        if (strtolower($this->jpgFile->extension) == "jpg" || strtolower($this->jpgFile->extension) == "jpeg" ) {
            $this->jpgFile->saveAs($url . $name);
            return $name;
        }
        return false;
    }
}