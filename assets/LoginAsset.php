<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'login/assets/plugins/bootstrap/css/bootstrap.min.css',
        'login/assets/plugins/bootstrap/css/bootstrap-responsive.min.css',
        'login/assets/plugins/font-awesome/css/font-awesome.min.css',
        'login/assets/css/style-metro.css',
        'login/assets/css/style.css',
        'login/assets/css/style-responsive.css',
        'login/assets/css/themes/default.css" rel="stylesheet',
        'login/assets/plugins/uniform/css/uniform.default.css',
        'login/assets/plugins/select2/select2_metro.css',
        'login/assets/css/pages/login-soft.css',
    ];
    public $js = [
        'login/assets/plugins/jquery-1.10.1.min.js',
        'login/assets/plugins/jquery-migrate-1.2.1.min.js',
        'login/assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js"',
        'login/assets/plugins/bootstrap/js/bootstrap.min.js',
        'login/assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js',
        'login/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
        'login/assets/plugins/jquery.blockui.min.js"',
        'login/assets/plugins/jquery.cookie.min.js',
        'login/assets/plugins/uniform/jquery.uniform.min.js"',
        'login/assets/plugins/jquery-validation/dist/jquery.validate.min.js',
        'login/assets/plugins/backstretch/jquery.backstretch.min.js',
        'login/assets/plugins/select2/select2.min.js',
        'login/assets/scripts/app.js',
        'login/assets/scripts/login-soft.js"',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
