<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap-rtl.min.css',
//        'css/bootstrap.min.css',
        'font-awesome/css/font-awesome.css',
        'css/site.css',
        'css/reset.css',
        'css/dropzone.css',
    ];
    public $js = [
        'js/bootstrap.js',
//        'js/carousel.js',
//        'js/Carousel.js',
        'js/jquery.rollingslider.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
