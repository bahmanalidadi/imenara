<?php

namespace app\controllers;

use app\modules\image_notice\models\ImageNotice;
use app\modules\menu\models\SiteMenu;
use app\modules\news\models\News;
use app\modules\news\models\NewsCategory;
use app\modules\newsletters\models\Newsletter;
use app\modules\notice\models\Notice;
use app\modules\poll\models\Poll;
use app\modules\poll\models\PollItem;
use app\modules\product\models\CategoryProp;
use app\modules\product\models\Product;
use app\modules\product\models\ProductCategory;
use app\modules\slider\models\Slider;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
//        $image_notice = ImageNotice::find()->orderBy('id DESC')->limit(1)->one();
        $latest_news = News::find()->orderBy('id DESC')->limit(5)->all();
//        $notice = Notice::find()->orderBy('id DESC')->limit(3)->all();
//        $more_view_news = News::find()->orderBy('views DESC')->limit(3)->all();
//        $categories = NewsCategory::find()->where('available = 1')->all();
        $sliders = Slider::find()->where(['available' => 1])->orderBy('position')->limit(4)->all();
//        $site_menus = SiteMenu::find()->orderBy('id')->limit(5)->all();
//        $poll = Poll::find()->where(['available' => 1])->orderBy('id DESC')->one();
//        $poll_items = $poll->pollItems;


//        print_r($notice);die;
        return $this->render('index',[
//            'image_notice' => $image_notice,
            'latest_news' => $latest_news,
//            'notice' =>$notice,
//            'categories' => $categories,
//            'more_view_news' => $more_view_news,
            'sliders' => $sliders,
//            'site_menus' => $site_menus,
//            'poll' => $poll,
//            'poll_items' => $poll_items,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionSendEmail()
    {

        $news_letter = new Newsletter();
        if (Yii::$app->request->post('email')){

            $news_letter->email = Yii::$app->request->post('email');
            if ($news_letter->save()){
                Yii::$app->response->format = Response::FORMAT_JSON;
                Yii::$app->response->statusCode = 200;
                return "ایمیل شما با موفقیت ثبت شد";
            }
            Yii::$app->response->statusCode = 400;
            return "ثبت ایمیل با مشکل مواجه شد";

        }
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        $this->layout = 'login';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionProductPage($id)
    {
        $product = Product::find()->where(['id' => $id])->one();
        return $this->render('product-page',[
                'product' => $product
            ]);
    }

    public function actionGetProperties()
    {
        $id = Yii::$app->request->post('id');
        if ($id){
            $category = ProductCategory::find()->where(['id' =>$id])->one();
            if ($category){
                Yii::$app->response->format = Response::FORMAT_JSON;
                Yii::$app->response->statusCode = 200;
                return $category->categoryProps;

            }
            Yii::$app->response->statusCode = 400;
            return "دسته بندی یافت نشد";

        }
    }



    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
