<?php
namespace app\classes;
use DateTime;
use DateTimeZone;
use yii\base\Exception;

/**
 * Jalali Date Time Class, supports years higher than 2038
 * by: Ramin Farmani
 *
 * Requires PHP >= 5.2
 *
 * PHP's default 'date' function does not support years higher than
 * 2038. Intorduced in PHP5, DateTime class supports higher years
 * and also invalid date entries.
 * Also, Persian users are using classic 'jdate' function for years now
 * and beside the fact that it's amazing and also helped me to write this
 * one, it's really out of date, and can't be used in modern real world
 * web applications as it is completely written in functions.
 *
 * Copyright (C) 2011  Ramin Farmani (http://www.farmani.ir)
 * Part of Cryptext Framework (cryptext.org) by Cryptext Alternatvie
 *
 * Original Jalali to Gregorian (and vice versa) convertor:
 * Copyright (C) 2000  Roozbeh Pournader and Mohammad Toossi
 *
 * List of supported timezones can be found here:
 * http://www.php.net/manual/en/timezones.php
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @package    JDateTime
 * @author     Ramin Farmani <ramin.farmani@gmail.com>
 * @copyright  2003-2011 Ramin Farmani
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link       http://cryptext.org/package/JDateTime
 * @see        DateTime
 * @version    1.0.0
 */
class UsableFunctions
{
    public static function get_jalali_date($timestamp, $format='Y-m-d H:i:s')
    {
        $jdate = new JDateTime(false);
        return $jdate->date($format, $timestamp);
    }

    public static function conver_to_string_date($timestamp)
    {
        $sub_value = time() - $timestamp;
        if($sub_value >= 86400)
        {
            return round($sub_value / 86400, 0) .' روز قبل';
        }
        elseif($sub_value >= 3600)
        {
            return round($sub_value / 3600, 0) .' ساعت قبل';
        }
        elseif($sub_value >= 60)
        {
            return round($sub_value / 60, 0) .' دقیقه قبل';
        }
        else{
            return 'چند لحظه پیش';
        }
    }

    public static function start_this_month_timestamp($jd)
    {
        $jd_array = explode('-', $jd);
        $jdate = new JDateTime(false);
        $result = $jdate->toGregorian($jd_array[0], $jd_array[1], 1);

        return strtotime(implode('-',$result));
    }

    public static function start_end_this_month_timestamp()
    {
        $jdate = new JDateTime(false);
        $current_date = $jdate->date('Y-m-d');
        $jd_array = explode('-', $current_date);
        $start = $jdate->toGregorian($jd_array[0], $jd_array[1], 1);
        $start_timestamp = strtotime(implode('-',$start));
        $end = $jdate->toGregorian($jd_array[0], $jd_array[1]+1, 1);
        $end_timestamp = strtotime(implode('-',$end));
        return [
            'start' => $start_timestamp,
            'end' => $end_timestamp
        ];
    }


    public static function excel_report($query, $titles)
    {
        $file = \Yii::createObject([
            'class' => 'codemix\excelexport\ExcelFile',
            'sheets' => [
                'Customers' => [   // Name of the excel sheet
                    'data' => $query,

                    // Set to `false` to suppress the title row
                    'titles' => $titles,
                ]

            ]
        ]);
        $file->send('excel_report.xlsx');
    }

    public static function add_watermark($image_file)
    {
        if(strpos($image_file,'.jpg') > 0 || 1) {
            $watermark = imagecreatefrompng('images/watermark/new_logo_2.png');

            $watermark_width = imagesx($watermark);
            $watermark_height = imagesy($watermark);

            $image = imagecreatefromjpeg($image_file);
            $size = getimagesize($image_file);
            $dest_x = $size[0] - $watermark_width - 5;
            $dest_y = $size[1] - $watermark_height - 5;

            imagecopy($image, $watermark, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height);

            $filename = $image_file;
            imagejpeg($image, $filename);

            imagedestroy($image);
            imagedestroy($watermark);
            return $filename;
        }

        return $image_file;
    }

    public static function send_sms($mobile, $token)
    {
        $url = 'https://api.kavenegar.com/v1/696139554F2F5563415975306945646E476A70796E513D3D/verify/lookup.json';

        $params = 'receptor='.$mobile;
        $params .= '&token='.$token;
        $params .= '&template=verify';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);
        curl_close ($ch);

        $result = json_decode($server_output);
        if($result->return->status  == 200)
        {
            return true;
        }

        return false;


    }

    public static function substr_by_words_length($text, $length){
        $content = strip_tags($text);
        $str = implode(' ', array_slice(explode(' ', $content), 0, $length));
        $str .= (strlen($str) < strlen($content)) ? " ..." : "";
        return $str;
    }

}